-----------------------------------------------------------------------------------------------
BEAT CHOPPA
-----------------------------------------------------------------------------------------------
INSTALLATION - UTILISATEUR TECHNIQUE:
Version d'Eclipse utilis�e pour le d�veloppement: Eclipse Neon 2
Fonctionne avec la derni�re version de Java

Pour pouvoir d�velopper dans Eclipse, il ne suffit que d'ouvrir comme espace de travail
le dossier �beatchoppa�.

La seule librairie utilis�e est TarsosDSP et elle est compress�e dans un fichier JAR sous
\beatchoppa\BeatChoppa\lib\TarsosDSP-latest.jar

SEUL LES FICHIERS AUDIO DE FORMAT WAV MONO CANAL SONT ACCEPT�S. 
L'option la plus simple pour les tests est donc d'utiliser les fichiers plac�s � la racine
du dossier beatchoppa. 

------------------------------------------------------------------------------------------------
INSTALLATION - UTILISATEUR NON TECHNIQUE:

Le programme fonctionne sans d�pendances externes, il ne suffit que d'�x�cuter le fichier
beatchoppa.jar. Cependant, seul les fichiers de format WAVE mono canal sont pris en charge.