package syntheziser;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.Set;
import java.util.TreeMap;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import audioEffects.BcAudioEffectsManager;
import audioPlayer.BcPlayer;
import audioPlayer.BcPlayerState;
import be.tarsos.dsp.AudioProcessor;
import sineWavePanel.BcSineWaveInterface;
import util.Util;
import javax.swing.border.EtchedBorder;
import javax.swing.JRadioButton;
import java.awt.TextArea;

/**
 * Cette classe est le point de d�part du logiciel. L'interface y est g�r� et elle instancie les �l�ments importants 
 * qui permettent au programme de fonctionner comme le lecteur audio et les paneaux permettant de voir le signal audio. 
 * */

public class BcSynthesizer extends JFrame {

	private DefaultComboBoxModel<String> synthesisEffectsCBModel;
	private  TreeMap<String, AudioProcessor> processorsAvailable;
	private BcPlayer player;
	private Listener lst;
	private BcAudioEffectsManager effectsManager;
	private BcSineWaveInterface panelWaveForm;
	private static final DecimalFormat df = new DecimalFormat( "#0.000" );
	private Timer timeThread;
	private final int framesPerSecond = 50;
	private final int refreshInterval = framesPerSecond/1000;
	private final JFileChooser fc = new JFileChooser();
	
	private JMenuItem menuItmOpenFile;
	private JPanel contentPane;
	private JTextField tfStartInSeconds;
	private JTextField tfEndInSeconds;
	private JButton btnPlaypause;
	private JSlider slidergain;
	private JCheckBox chckbxLoop;
	private JLabel lblDuration;
	private JLabel lblStartTimeInSeconds;
	private JLabel lblEndTimeInSeconds;
	private JLabel lbltimetracker; 
	private JMenuItem menuItmSaveSample;
	private JScrollPane scrollPaneEffects;
	private JTable table;
	private JComboBox cBAvailableEffects;
	private DefaultComboBoxModel<String> availableEffectsCBModel;	
	private JComboBox cBSynthesisEffects;
	private JCheckBox chckbxKeepWithCursor;
	private JLabel lblEndTimeInSamples;
	private JLabel lblStartTimeInSamples;
	private JTextField tfStartInSamples;
	private JTextField tfEndInSamples;
	private JCheckBox chckbxDisplaySampleSineWave;
	private JLabel lblLenghtInSamples;
	private JLabel lblFile;
	private JLabel lblFormat;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
                try {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                break;
            }
        }
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BcSynthesizer frame = new BcSynthesizer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 * @throws UnsupportedAudioFileException 
	 */
	public BcSynthesizer() throws UnsupportedAudioFileException, IOException {
		setTitle("BeatChoppa");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1200, 700);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		menuItmOpenFile = new JMenuItem("Open File");
		menuItmOpenFile.setBorder(new LineBorder(new Color(0, 0, 0)));
		menuItmOpenFile.setHorizontalAlignment(SwingConstants.CENTER);
		menuItmOpenFile.setHorizontalTextPosition(SwingConstants.CENTER);
		menuItmOpenFile.setMaximumSize(new Dimension(120, 32767));
		menuItmOpenFile.setSize(new Dimension(50, 0));
		menuBar.add(menuItmOpenFile);
		
		menuItmSaveSample = new JMenuItem("Save sample");
		menuItmSaveSample.setBorder(new LineBorder(new Color(0, 0, 0)));
		menuItmSaveSample.setHorizontalAlignment(SwingConstants.CENTER);
		menuItmSaveSample.setHorizontalTextPosition(SwingConstants.CENTER);
		menuItmSaveSample.setMaximumSize(new Dimension(120, 32767));
		menuBar.add(menuItmSaveSample);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAvailableEffects = new JLabel("Add synthesis effect:");
		lblAvailableEffects.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAvailableEffects.setBounds(991, 2, 183, 37);
		contentPane.add(lblAvailableEffects);
		
		availableEffectsCBModel = new DefaultComboBoxModel<String>();
		cBAvailableEffects = new JComboBox(availableEffectsCBModel);
		cBAvailableEffects.setBounds(833, 35, 341, 30);
		contentPane.add(cBAvailableEffects);
		
		JLabel lblBeginning = new JLabel("Start(sec):");
		lblBeginning.setBounds(618, 18, 66, 37);
		contentPane.add(lblBeginning);
		
		JLabel lblEnd = new JLabel("End (sec):");
		lblEnd.setBounds(618, 50, 66, 37);
		contentPane.add(lblEnd);
		
		tfStartInSeconds = new JTextField();
		tfStartInSeconds.setBounds(683, 26, 66, 23);
		contentPane.add(tfStartInSeconds);
		tfStartInSeconds.setColumns(10);
		
		tfEndInSeconds = new JTextField();
		tfEndInSeconds.setBounds(683, 58, 66, 23);
		contentPane.add(tfEndInSeconds);
		tfEndInSeconds.setColumns(10);
		
		chckbxLoop = new JCheckBox("loop");
		chckbxLoop.setBounds(548, 100, 52, 23);
		contentPane.add(chckbxLoop);
		
		slidergain = new JSlider();
		slidergain.setMaximum(200);
		slidergain.setValue(100);
		slidergain.setBounds(51, 97, 149, 26);
		contentPane.add(slidergain);
		
		JLabel lblGain = new JLabel("Gain:");
		lblGain.setBounds(10, 99, 29, 25);
		contentPane.add(lblGain);
		
		scrollPaneEffects = new JScrollPane();
		scrollPaneEffects.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		scrollPaneEffects.setBounds(10, 135, 1164, 100);
		contentPane.add(scrollPaneEffects);
		
		lblStartTimeInSeconds = new JLabel("--");
		lblStartTimeInSeconds.setText("00:00:000");
		lblStartTimeInSeconds.setHorizontalTextPosition(SwingConstants.CENTER);
		lblStartTimeInSeconds.setHorizontalAlignment(SwingConstants.CENTER);
		lblStartTimeInSeconds.setBounds(741, 18, 70, 36);
		contentPane.add(lblStartTimeInSeconds);
		
		lblEndTimeInSeconds = new JLabel("--");
		lblEndTimeInSeconds.setText("00:00:000");
		lblEndTimeInSeconds.setHorizontalTextPosition(SwingConstants.CENTER);
		lblEndTimeInSeconds.setHorizontalAlignment(SwingConstants.CENTER);
		lblEndTimeInSeconds.setBounds(742, 50, 69, 36);
		contentPane.add(lblEndTimeInSeconds);
		
		JLabel lblEndsamples = new JLabel("End (samples):");
		lblEndsamples.setBounds(340, 49, 85, 37);
		contentPane.add(lblEndsamples);
		
		JLabel lblStartsamples = new JLabel("Start(samples): ");
		lblStartsamples.setBounds(340, 17, 97, 37);
		contentPane.add(lblStartsamples);
		
		JLabel lblSynthesisEffects = new JLabel("Synthesis effect:");
		lblSynthesisEffects.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSynthesisEffects.setBounds(991, 61, 183, 37);
		contentPane.add(lblSynthesisEffects);
		
		chckbxKeepWithCursor = new JCheckBox("Stick with cursor");
		chckbxKeepWithCursor.setBounds(212, 100, 119, 23);
		contentPane.add(chckbxKeepWithCursor);
		
		tfStartInSamples = new JTextField();
		tfStartInSamples.setHorizontalAlignment(SwingConstants.RIGHT);
		tfStartInSamples.setText("0");
		tfStartInSamples.setColumns(10);
		tfStartInSamples.setBounds(437, 26, 81, 23);
		contentPane.add(tfStartInSamples);
		
		tfEndInSamples = new JTextField();
		tfEndInSamples.setHorizontalAlignment(SwingConstants.RIGHT);
		tfEndInSamples.setText("0");
		tfEndInSamples.setColumns(10);
		tfEndInSamples.setBounds(437, 58, 81, 23);
		contentPane.add(tfEndInSamples);
		
		
		lblEndTimeInSamples = new JLabel("0");
		lblEndTimeInSamples.setHorizontalTextPosition(SwingConstants.CENTER);
		lblEndTimeInSamples.setHorizontalAlignment(SwingConstants.CENTER);
		lblEndTimeInSamples.setBounds(524, 51, 89, 36);
		contentPane.add(lblEndTimeInSamples);
		
		lblStartTimeInSamples = new JLabel("0");
		lblStartTimeInSamples.setHorizontalTextPosition(SwingConstants.CENTER);
		lblStartTimeInSamples.setHorizontalAlignment(SwingConstants.CENTER);
		lblStartTimeInSamples.setBounds(524, 19, 89, 36);
		contentPane.add(lblStartTimeInSamples);
		
		synthesisEffectsCBModel = new DefaultComboBoxModel<String>();
		cBSynthesisEffects = new JComboBox(synthesisEffectsCBModel);
		cBSynthesisEffects.setBounds(833, 96, 341, 30);
		contentPane.add(cBSynthesisEffects);
		
		btnPlaypause = new JButton("Play");
		btnPlaypause.setBounds(618, 93, 59, 37);
		contentPane.add(btnPlaypause);
		
		lblDuration = new JLabel("- - : - -");
		lblDuration.setBounds(762, 100, 38, 23);
		contentPane.add(lblDuration);
		lblDuration.setHorizontalTextPosition(SwingConstants.LEFT);
		lblDuration.setHorizontalAlignment(SwingConstants.LEFT);
		
		JLabel lblLenghtSamples = new JLabel("Lenght (samples):");
		lblLenghtSamples.setBounds(340, 93, 101, 37);
		contentPane.add(lblLenghtSamples);
		
		lblLenghtInSamples = new JLabel("0");
		lblLenghtInSamples.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLenghtInSamples.setBounds(437, 102, 79, 19);
		contentPane.add(lblLenghtInSamples);
		
		JLabel lblBackSlash = new JLabel(" / ");
		lblBackSlash.setBounds(747, 102, 10, 19);
		contentPane.add(lblBackSlash);
		
		lbltimetracker = new JLabel("- - : - -");
		lbltimetracker.setBounds(693, 102, 49, 19);
		contentPane.add(lbltimetracker);
		lbltimetracker.setHorizontalAlignment(SwingConstants.RIGHT);
		
		lblFile = new JLabel("");
		lblFile.setBounds(10, 12, 318, 37);
		contentPane.add(lblFile);
		
		lblFormat = new JLabel("");
		lblFormat.setBounds(10, 42, 318, 37);
		contentPane.add(lblFormat);
		
		
		//Cr�ation du manager
		effectsManager = new BcAudioEffectsManager();
		
		//Indiquer les effets qui sont disponibles
		for(String effects:effectsManager.getAvailableProcessors()) {
			availableEffectsCBModel.addElement(effects);
		}
		
		lst = new Listener();
		menuItmOpenFile.addActionListener(lst);

		//La liste qui contient les processeurs cr��s
		processorsAvailable = new TreeMap<String, AudioProcessor>();
	}
	
	public boolean openFile(){
		fc.setCurrentDirectory(new java.io.File("."));
	    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = fc.showOpenDialog(this);
		File file = null;
		if (returnVal == JFileChooser.APPROVE_OPTION) {
            file = fc.getSelectedFile();
        } else {
        	return false;
        }
		AudioFileFormat fileFormat;
		try {
			fileFormat = AudioSystem.getAudioFileFormat(file);
		} catch (UnsupportedAudioFileException | IOException e) {
			JOptionPane.showMessageDialog(this, "Only works with a WAV MonoChanel File");
			return false;
		}
		AudioFormat audioFormat = fileFormat.getFormat();
		if(fileFormat.getType() != AudioFileFormat.Type.WAVE){
			JOptionPane.showMessageDialog(this, "Only works with a WAV MonoChanel File");
			return false;
		} else if(audioFormat.getChannels() != 1){
			JOptionPane.showMessageDialog(this, "Only works with a WAV MonoChanel File");
			return false;
		} else{
			removeListeners();
			if(player != null){
				player.eject();
				player = null;
			}
			if(timeThread != null){
				timeThread.stop();
			}
			if(panelWaveForm != null){
				contentPane.remove(panelWaveForm);
				panelWaveForm = null;
			}
			scrollPaneEffects.setViewportView(null);
			cBSynthesisEffects.removeAllItems();
			cBSynthesisEffects.setSelectedItem(null);
			
			player = new BcPlayer(this, file);
			player.setLooping(chckbxLoop.isSelected());
			effectsManager = new BcAudioEffectsManager(player);
		
			panelWaveForm = new BcSineWaveInterface(10, 246, 1164, 378, player, file);
			contentPane.add(panelWaveForm);
			panelWaveForm.keepWithCursor(chckbxKeepWithCursor.isSelected());
			
			initializeListeners();
			initializeInfos();
			initializetimeThread();
			return true;
		}
	}
	
	public boolean saveFile(){
		
		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		fc.setCurrentDirectory(new java.io.File(""));
		
        File fichier = new File("mySample.wav"); // Permet de donner un nom par d�faut
		fc.setSelectedFile(fichier);
		
		int returnVal = fc.showSaveDialog(this);
		File file = null;
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			if(fc.getSelectedFile().getName() == null || fc.getSelectedFile().getName() == "."){
				return false;
			} else {
				file = fc.getSelectedFile();
				if (!file.getName().toLowerCase().endsWith(".wav")) {
			        file = new File(fc.getSelectedFile() + ".wav");
			     }
				player.record(file);
				return true;
			}
        } else {
        	JOptionPane.showMessageDialog(this, "The selected directory doesn't work...");
        	return false;
        }
	}
	
	public void initializetimeThread(){
		//On cr�e le thread qui va mettre la barre de progression et le temps � jour
		effectsManager.setPlayer(player);
		timeThread = new Timer(refreshInterval, lst);
		timeThread.start();
	}
	
	public void removeListeners(){
		slidergain.removeChangeListener(lst);
		tfStartInSeconds.removeActionListener(lst);
		tfEndInSeconds.removeActionListener(lst);
		chckbxLoop.removeActionListener(lst);
		menuItmSaveSample.removeActionListener(lst);
		cBAvailableEffects.removeActionListener(lst);
		cBSynthesisEffects.removeActionListener(lst);
		chckbxKeepWithCursor.removeActionListener(lst);
		btnPlaypause.removeActionListener(lst);
		tfStartInSamples.removeActionListener(lst);
		tfEndInSamples.removeActionListener(lst);
	}
	
	public void initializeListeners(){
		slidergain.addChangeListener(lst);
		tfStartInSeconds.addActionListener(lst);
		tfEndInSeconds.addActionListener(lst);
		chckbxLoop.addActionListener(lst);
		menuItmSaveSample.addActionListener(lst);
		cBAvailableEffects.addActionListener(lst);
		cBSynthesisEffects.addActionListener(lst);
		chckbxKeepWithCursor.addActionListener(lst);
		btnPlaypause.addActionListener(lst);
		tfStartInSamples.addActionListener(lst);
		tfEndInSamples.addActionListener(lst);
	}
	
	public void initializeInfos(){
		lblDuration.setText(String.valueOf((Util.secondsToString(player.getTimeManager().getDurationInSeconds()))));
		lbltimetracker.setText(Util.secondsToString(player.getTimeManager().getCurrentTimeInSeconds()));
		lblLenghtInSamples.setText(String.valueOf(player.getTimeManager().getLenghtInSamples()));
		lblFile.setText(player.getFileName());
		lblFormat.setText(player.getFormat());
		updateElementsRelatedToTime();
	}
	
	public JScrollPane getScrollPane(){
		return scrollPaneEffects;
	}

	public BcAudioEffectsManager getEffectsManager() {
		return effectsManager;
	}
	
	private void updateTime(){
		lbltimetracker.setText(Util.secondsToString(player.getTimeManager().getCurrentTimeInSeconds()));
		panelWaveForm.updateTime();
	}
	
	public void updateSynthesisEffectsList(){
		cBSynthesisEffects.removeActionListener(lst);
		synthesisEffectsCBModel.removeAllElements();
		processorsAvailable = effectsManager.getInstantiatedProcessors();
		Set<String> keys = processorsAvailable.keySet();
		for(String effect:keys) {
			synthesisEffectsCBModel.addElement(effect);
		}
		cBSynthesisEffects.addActionListener(lst);
	}
	
	public void selectLasteffect(){
		if(synthesisEffectsCBModel.getSize() > 1){
			synthesisEffectsCBModel.setSelectedItem(synthesisEffectsCBModel.getElementAt(synthesisEffectsCBModel.getSize()-1));
		}
	}
	
	private void createEffectPanel(String selectedEffect){
		scrollPaneEffects.setViewportView(effectsManager.createEffectPanel(processorsAvailable.get(selectedEffect), BcSynthesizer.this));
	}
	
	public BcPlayer getPlayer(){
		return this.player;
	}
	
	public void updateElementsRelatedToTime(){
		tfStartInSeconds.setText(String.valueOf((df.format(player.getTimeManager().getStartInSeconds()))));
		lblStartTimeInSeconds.setText(Util.milisecondsToString(player.getTimeManager().getStartInSeconds()));
		
		lblEndTimeInSeconds.setText(Util.milisecondsToString(player.getTimeManager().getEndInSeconds()));
		tfEndInSeconds.setText(String.valueOf(df.format(player.getTimeManager().getEndInSeconds())));

		lblStartTimeInSamples.setText(String.valueOf(player.getTimeManager().getStartInSamples()));
		tfStartInSamples.setText(String.valueOf(player.getTimeManager().getStartInSamples()));
		
		lblEndTimeInSamples.setText(String.valueOf(player.getTimeManager().getEndInSamples()));
		tfEndInSamples.setText(String.valueOf(player.getTimeManager().getEndInSamples()));
		
		panelWaveForm.redraw();
	}
	
	public void createAProcessor(){
		String processor = cBAvailableEffects.getSelectedItem().toString();
		String createdEffectName  = effectsManager.addAudioProcessor(processor);
		updateSynthesisEffectsList();
		createEffectPanel(createdEffectName);
	}
	
	/** Ces quatre m�thodes permettent de d�terminer le d�but et la fin de l'extrait audio s�lectionn�. */
	public void setStartInSeconds(){
		String start = tfStartInSeconds.getText();
		if(Util.sanitizeStartOrEndInSeconds(BcSynthesizer.this, "start", start, player.getTimeManager().getStartInSeconds(), player.getTimeManager().getEndInSeconds(), player.getTimeManager().getDurationInSeconds())){
			player.getTimeManager().setStartInSeconds(Double.parseDouble(start));
		}
	}
	
	public void setEndInSeconds(){
		String end = tfEndInSeconds.getText();
		if(Util.sanitizeStartOrEndInSeconds(BcSynthesizer.this, "end", end, player.getTimeManager().getStartInSeconds(), player.getTimeManager().getEndInSeconds(), player.getTimeManager().getDurationInSeconds())){
			player.getTimeManager().setEndInSeconds(Double.parseDouble(end));
		}
	}
	
	public void setStartInSamples(){
		String start = tfStartInSamples.getText();
		if(Util.sanitizeStartOrEndInSamples(BcSynthesizer.this, "start", start, player.getTimeManager().getStartInSamples(), player.getTimeManager().getEndInSamples(), player.getTimeManager().getLenghtInSamples())){
			player.getTimeManager().setStartInSamples((Long.parseLong(start)));
		}
	}
	
	public void setEndInSamples(){
		String end = tfEndInSamples.getText();
		if(Util.sanitizeStartOrEndInSamples(BcSynthesizer.this, "end", end, player.getTimeManager().getStartInSamples(), player.getTimeManager().getEndInSamples(), player.getTimeManager().getLenghtInSamples())){
			player.getTimeManager().setEndInSamples((Long.parseLong(end)));
		}
	}
	/****************************************************************/
	
	/**Cette fonction permet de d�marrer ou d'arr�ter la chanson*/
	public void togglePlayPause(){
		if(player.getState() == BcPlayerState.PLAYING ){
			player.pause();					
		} else if (player.getState() == BcPlayerState.PAUZED || player.getState() == BcPlayerState.STOPPED){
			player.play();		
		}
	}
	
	/**Cette fonction permet de d�termin� l'amplitude du signal. La modification du ''Gain'' se manifeste par une augmentation et 
	 * une diminuation du son, mais � plus d'implication que de simplement le mofidier. */
	public void setGain(){
		double interval = slidergain.getMaximum() - slidergain.getMinimum();
		double middle = interval / 2;
		double slider = slidergain.getValue();
		double newGain = 0;
		if(slider == middle){
			newGain = 1.0;
			player.setGain(newGain);
		} else if (slider > middle){
			newGain = 1.0 + ((player.getMaximumGain() - 1.0) * (slider - middle)/middle);
			player.setGain(newGain);
		} else if (slider < middle){
			newGain = slider/middle;
			player.setGain(newGain);
		}
	}
	
	public DefaultComboBoxModel<String> getSynthesisEffectsCBModel() {
		return synthesisEffectsCBModel;
	}

	private class Listener implements ActionListener, ChangeListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == btnPlaypause){
				togglePlayPause();
			} else if(e.getSource() == tfStartInSeconds){
				setStartInSeconds();
			} else if(e.getSource() == tfEndInSeconds){
				setEndInSeconds();
			} else if(e.getSource() == timeThread){
				updateTime();
			} else if(e.getSource() == chckbxLoop){
				player.setLooping(chckbxLoop.isSelected());
			} else if(e.getSource() == menuItmSaveSample){
				saveFile();
			} else if(e.getSource() == cBAvailableEffects){
				createAProcessor();
			} else if(e.getSource() == cBSynthesisEffects){
				String selectedEffect = cBSynthesisEffects.getSelectedItem().toString();
				createEffectPanel(selectedEffect);
			} else if(e.getSource() == chckbxKeepWithCursor){
				panelWaveForm.keepWithCursor(chckbxKeepWithCursor.isSelected());
			} else if(e.getSource() == tfStartInSamples){
				setStartInSamples();
			} else if(e.getSource() == tfEndInSamples){
				setEndInSamples();
			} else if(e.getSource() == menuItmOpenFile){
				openFile();
			}
		}

		@Override
		public void stateChanged(ChangeEvent e) {
			if(e.getSource() == slidergain){
				setGain();
			}
		}
	}
}


