package audioEffects;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import syntheziser.BcSynthesizer;
import tarsosDSPModifiedClasses.ModifiedAmplitudeLFO;
import util.Util;

public class BcAmplitudeLFOPanel extends BcAudioEffectPanel {
	private AmplitudeLFOListener listener;
	
	public BcAmplitudeLFOPanel(ModifiedAmplitudeLFO processor, BcSynthesizer container) {
		super(processor, container);
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), BcAudioEffectsManager.AmplitudeLFOInfo.getProcessorName(), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		//Connection de l'�couteur
		listener = new AmplitudeLFOListener();
		tfParam1.addActionListener(listener);
		tfParam2.addActionListener(listener);
		
		//Initialiser les boites avec les valeurs du processeur
		tfParam1.setText(String.valueOf(((ModifiedAmplitudeLFO)processor).getFrequency()));
		tfParam2.setText(String.valueOf(((ModifiedAmplitudeLFO)processor).getScaleParameter()));
		
		//Modification des label par d�faut
		lblParam1.setText("Frequency (Hz): ");
		lblParam2.setText("Scale ([0, 1]):");
	}
	
	private class AmplitudeLFOListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			if(e.getSource() == tfParam1){ //Frequency
				String param = tfParam1.getText();
				if(Util.sanitizeStringNumericalValue(container, param)){
					((ModifiedAmplitudeLFO)processor).setFrequency(Double.parseDouble(param));;
				} else {
					tfParam1.setText(String.valueOf(((ModifiedAmplitudeLFO)processor).getFrequency()));
				}
			} else if(e.getSource() == tfParam2){ //Scale
				String param = tfParam2.getText();
				if(Util.sanitizeStringBetween1And0(container, param)){
					((ModifiedAmplitudeLFO)processor).setScaleParameter(Double.parseDouble(param));
				} else {
					tfParam2.setText(String.valueOf(((ModifiedAmplitudeLFO)processor).getScaleParameter()));
				}
			}
		}
	}

}
