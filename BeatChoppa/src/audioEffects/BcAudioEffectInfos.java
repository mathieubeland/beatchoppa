package audioEffects;

public class BcAudioEffectInfos {
	private String panel;
	private Class<?> panelClass;
	private String processorName;
	private Class<?> processorClass;
	
	public BcAudioEffectInfos(String panel, Class<?> panelClass, String processorName,
			Class<?> processorClass) {
		super();
		this.panel = panel;
		this.panelClass = panelClass;
		this.processorName = processorName;
		this.processorClass = processorClass;
	}
	
	public String getPanel() {
		return panel;
	}
	
	public void setPanel(String panel) {
		this.panel = panel;
	}
	
	public Class<?> getPanelClass() {
		return panelClass;
	}
	
	public void setPanelClass(Class<?> panelClass) {
		this.panelClass = panelClass;
	}

	public String getProcessorName() {
		return processorName;
	}

	public void setProcessorName(String processorName) {
		this.processorName = processorName;
	}
	
	public Class<?> getProcessorClass() {
		return processorClass;
	}
	
	public void setProcessorClass(Class<?> processorClass) {
		this.processorClass = processorClass;
	}
}