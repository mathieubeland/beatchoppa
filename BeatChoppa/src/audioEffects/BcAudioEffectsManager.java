package audioEffects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import audioPlayer.BcPlayer;
import audioPlayer.BcPlayerState;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.WaveformSimilarityBasedOverlapAdd.Parameters;
import syntheziser.BcSynthesizer;
import tarsosDSPModifiedClasses.ModifiedAmplitudeLFO;
import tarsosDSPModifiedClasses.ModifiedBandPass;
import tarsosDSPModifiedClasses.ModifiedDelayEffect;
import tarsosDSPModifiedClasses.ModifiedFlangerEffect;
import tarsosDSPModifiedClasses.ModifiedHighPass;
import tarsosDSPModifiedClasses.ModifiedLowPassSP;
import tarsosDSPModifiedClasses.ModifiedNoiseGenerator;
import tarsosDSPModifiedClasses.ModifiedOptimizedGranulator;
import tarsosDSPModifiedClasses.ModifiedSineGenerator;
import tarsosDSPModifiedClasses.ModifiedWaveformSimilarityBasedOverlapAdd;
public class BcAudioEffectsManager {
	private BcPlayer player;
	private Hashtable<Class<?>, Integer> processorsIdGenerator;
	private Hashtable<AudioProcessor, Integer> processorsIds;
	private ModifiedWaveformSimilarityBasedOverlapAdd WSOLAdummie;
	private ModifiedOptimizedGranulator GranulatorDummie;
	
	public BcAudioEffectsManager(BcPlayer player) {
		this.player = player;
		intitialiseIdGenerator();
	}
	
	public BcAudioEffectsManager() {}
	
	public void setPlayer(BcPlayer player){
		this.player = player;
		intitialiseIdGenerator();
	}
	
	//Pour ajouter un processeur et le panel correspondant, il y a 4 �tapes.
	
	// 1. LES INFORMATIONS DE TOUS LES EFFETS DISPONIBLES DANS DES CONTENEURS
	//FlangerEffect
	protected static final BcAudioEffectInfos FlangerEffectInfo = new BcAudioEffectInfos("BcFlangerEffectPanel", BcFlangerEffectPanel.class, "Flanger Effect", ModifiedFlangerEffect.class);
	//SineGenerator
	protected static final BcAudioEffectInfos SineGeneratorInfo = new BcAudioEffectInfos("BcSineGeneratorPanel", BcSineGeneratorPanel.class, "Sine Generator", ModifiedSineGenerator.class);
	//WaveformSimilarityBasedOverlapAdd
	protected static final BcAudioEffectInfos WaveformSimilarityBasedOverlapAddInfo = new BcAudioEffectInfos("BcWaveformSimilarityBasedOverlapAddPanel", BcWaveformSimilarityBasedOverlapAddPanel.class, "Time Stretching",ModifiedWaveformSimilarityBasedOverlapAdd.class);
	//DelayEffectInfo
	protected static final BcAudioEffectInfos DelayEffectInfo = new BcAudioEffectInfos("BcDelayEffectPanel", BcDelayEffectPanel.class, "Delay Effect", ModifiedDelayEffect.class);
	//AmplitudeLFO
	protected static final BcAudioEffectInfos AmplitudeLFOInfo = new BcAudioEffectInfos("BcAmplitudeLFOPanel", BcAmplitudeLFOPanel.class, "Amplitude Low Frequency Oscillator", ModifiedAmplitudeLFO.class);
	//LowPassSP
	protected static final BcAudioEffectInfos LowPassSPInfo = new BcAudioEffectInfos("BcLowPassSPPanel", BcLowPassSPPanel.class, "Low-Pass Filter", ModifiedLowPassSP.class);
	//HighPass
	protected static final BcAudioEffectInfos HighPassInfo = new BcAudioEffectInfos("BcHighPassPanel", BcHighPassPanel.class, "High-Pass Filter", ModifiedHighPass.class);
	//BandPass
	protected static final BcAudioEffectInfos BandPassInfo = new BcAudioEffectInfos("BcBandPassFilterPanel", BcBandPassFilterPanel.class, "Band Pass Filter", ModifiedBandPass.class);
	//NoiseGenerator
	protected static final BcAudioEffectInfos NoiseGeneratorInfo = new BcAudioEffectInfos("BcNoiseGeneratorPanel", BcNoiseGeneratorPanel.class, "Noise Generator", ModifiedNoiseGenerator.class);		
	//NoiseGenerator
	protected static final BcAudioEffectInfos GranulatorInfo = new BcAudioEffectInfos("BcGranulatorPanel", BcGranulatorPanel.class, "Granulator", ModifiedOptimizedGranulator.class);	
	
	//2. CHAQUE EFFET EST MIS DANS LA LISTE
	private static final List<BcAudioEffectInfos> availableEffects = Arrays.asList(
			FlangerEffectInfo,
			SineGeneratorInfo,
			WaveformSimilarityBasedOverlapAddInfo,
			DelayEffectInfo,
			AmplitudeLFOInfo,
			LowPassSPInfo,
			HighPassInfo,
			BandPassInfo,
			NoiseGeneratorInfo,
			GranulatorInfo
	);
	
	//3. PERMET DE CR�ER LES AUDIOPROCESSORS ET DE LEUR ATTRIBUER UN ID
	public String addAudioProcessor(String processor) {
		AudioProcessor temp = null;
		if(processor.equals(FlangerEffectInfo.getProcessorName())){
			temp = new ModifiedFlangerEffect(0.02,0.5,player.getSampleRate(),0.5);
		}if(processor.equals(SineGeneratorInfo.getProcessorName())){
			temp = new ModifiedSineGenerator();	
		}if(processor.equals(WaveformSimilarityBasedOverlapAddInfo.getProcessorName())){
			player.setTimeStretch(true);
			if(WSOLAdummie != null){
				processorsIds.remove(WSOLAdummie);
				WSOLAdummie = null;
			}
			WSOLAdummie = new ModifiedWaveformSimilarityBasedOverlapAdd(Parameters.slowdownDefaults(1.0,44100));//On cr�e un processeur bidon
			processorsIds.put(WSOLAdummie, 0); //Le WSOLA est un processeur unique qui � toujours le m�me id.
		}if(processor.equals(DelayEffectInfo.getProcessorName())){
			temp = new ModifiedDelayEffect(1.5, 0.4, player.getSampleRate()); 
		}if(processor.equals(AmplitudeLFOInfo.getProcessorName())){
			temp = new ModifiedAmplitudeLFO();
		}if(processor.equals(LowPassSPInfo.getProcessorName())){
			temp = new ModifiedLowPassSP(1500, (float) player.getSampleRate());
		}if(processor.equals(HighPassInfo.getProcessorName())){
			temp = new ModifiedHighPass(500, (float) player.getSampleRate());
		}if(processor.equals(BandPassInfo.getProcessorName())){
			temp = new ModifiedBandPass(500, 500, (float) player.getSampleRate());
		}if(processor.equals(NoiseGeneratorInfo.getProcessorName())){
			temp = new ModifiedNoiseGenerator(0.0);
		}if(processor.equals(GranulatorInfo.getProcessorName())){
			player.setGrains(true);
			if(GranulatorDummie != null){
				processorsIds.remove(GranulatorDummie);
				GranulatorDummie = null;
			}
			GranulatorDummie = new ModifiedOptimizedGranulator(1024,44100);//On cr�e un processeur bidon
			processorsIds.put(GranulatorDummie, 0); //Le Granulator est un processeur unique qui � toujours le m�me id.
		}
		
		//Si le processeur est null, �a veut dire que le processeur existe d�j� dans le player (WaveformSimilarityBasedOverlapAdd ou Granulator)
		if(temp != null){
			//On attribut un id � chaque processeur pour pouvoir les distinguer dans le synth�tiseur.
			int id = attributeIDToProcessor(temp);
			//Le player reste toutefois ind�pendant. 
			player.addAudioProcessor(temp);
			return (processor + " " + String.valueOf(id));
		} else {
			if(player.getState() == BcPlayerState.PLAYING){
				player.pause();
				player.play();
			}
			return processor + " " + "0";
		}
	}
	
	//4. INSTANCIER LES PANNEAUX CORRESPONDANT AUX PROCESSEURS SELON LEUR CLASSE
	public BcAudioEffectPanel createEffectPanel(AudioProcessor processor, BcSynthesizer container){
		if(processor.getClass() == FlangerEffectInfo.getProcessorClass()){
			return new BcFlangerEffectPanel((ModifiedFlangerEffect)processor, container);
		} else if(processor.getClass() == SineGeneratorInfo.getProcessorClass()){
			return new BcSineGeneratorPanel((ModifiedSineGenerator)processor, container);
		} else if(processor.getClass() == WaveformSimilarityBasedOverlapAddInfo.getProcessorClass()){
			return new BcWaveformSimilarityBasedOverlapAddPanel(player.getWsola(), container);
		} else if(processor.getClass() == DelayEffectInfo.getProcessorClass()){
			return new BcDelayEffectPanel((ModifiedDelayEffect)processor, container);
		} else if(processor.getClass() == AmplitudeLFOInfo.getProcessorClass()){
			return new BcAmplitudeLFOPanel((ModifiedAmplitudeLFO)processor, container);
		} else if(processor.getClass() == LowPassSPInfo.getProcessorClass()){
			return new BcLowPassSPPanel((ModifiedLowPassSP)processor, container);
		} else if(processor.getClass() == HighPassInfo.getProcessorClass()){
			return new BcHighPassPanel((ModifiedHighPass)processor, container);
		} else if(processor.getClass() == BandPassInfo.getProcessorClass()){
			return new BcBandPassFilterPanel((ModifiedBandPass)processor, container);
		} else if(processor.getClass() == NoiseGeneratorInfo.getProcessorClass()){
			return new BcNoiseGeneratorPanel((ModifiedNoiseGenerator)processor, container);
		} else if(processor.getClass() == GranulatorInfo.getProcessorClass()){
			return new BcGranulatorPanel(GranulatorDummie, container);
		} else {
			return null;
		}
	}
	
	//Cette m�thode permet d'instancier la hastable qui permet de donner un id � chaque processeur cr�� selon son type de classe. 
	private  void intitialiseIdGenerator(){
		processorsIdGenerator = new Hashtable<Class<?>, Integer>();
		for(BcAudioEffectInfos processor:availableEffects){
			processorsIdGenerator.put(processor.getProcessorClass(), 0);
		}
		processorsIds = new Hashtable<AudioProcessor, Integer>();
	}
	
	//On attribut un id au processeur pass� en param�tre
	private int attributeIDToProcessor(AudioProcessor processor){
		Integer id = processorsIdGenerator.get(processor.getClass()) + 1;
		processorsIdGenerator.put(processor.getClass(), id); //On incr�mente le nombre de processeurs de ce type
		processorsIds.put(processor, id);
		return id;
	}
	
	//Permet de savoir les processeurs qui sont disponibles en order alphab�tique. Sert � initialiser la liste dans le synth�tiseur. 
	public List<String> getAvailableProcessors(){
		List<String> temp = new ArrayList<String>();
		for(BcAudioEffectInfos infos:availableEffects){
			temp.add(infos.getProcessorName());
		}
		temp.sort(String::compareToIgnoreCase);
		return temp;
	}
	
	//Permet d'afficher tous les processeurs. Retourne deux Strings: le type en String et le id du processeur. 
	public TreeMap<String, AudioProcessor> getInstantiatedProcessors(){
		TreeMap<String, AudioProcessor> temp = new TreeMap<String, AudioProcessor> ();
		
		Set<AudioProcessor> processors = processorsIds.keySet();
		for(AudioProcessor processor:processors) {
			BcAudioEffectInfos infos  = getInfosFromProcessorClass(processor);
			Integer id = processorsIds.get(processor);
			temp.put(infos.getProcessorName() + " " + id.toString(), processor);
		}
		return temp;
	}
	
	private BcAudioEffectInfos getInfosFromProcessorClass(AudioProcessor processor){
		for(BcAudioEffectInfos infos:availableEffects){
			if(infos.getProcessorClass() == processor.getClass()){
				return infos;
			}
		}
		return null;
	}
	
	public void deleteAudioProcessor(AudioProcessor processor) {
		processorsIds.remove(processor);
		try{
			player.getDispatcher().removeAudioProcessor(processor);
		} catch(NullPointerException e){
			//Le processeur n'avait pas encore �t� ajout� au processeur
		}
		player.deleteAudioProcessor(processor);
	}
	
	public double getTempo() {
		return player.getTempo();
	}

	public void setTempo(double tempo) {
		player.setTempo(tempo);
	}
	
	public void deleteProcessorID(AudioProcessor processor) {
		processorsIds.remove(processor);
	}
	
	public void deleteDummieWSOLA() {
		processorsIds.remove(WSOLAdummie);
		//On veut supprimer le wsola du dispatcher, mais pas du player. 
		try{
			player.getDispatcher().removeAudioProcessor(player.getWsola());
		} catch(NullPointerException e){
			//Le processeur n'a pas encore �t� ajout� dans le processeur. 
		}
		this.player.setTimeStretch(false);
	}
	
	public void deleteDummieGranulator() {
		processorsIds.remove(GranulatorDummie);
		//On veut supprimer le granulator du dispatcher, mais pas du player.
		try{
			player.getDispatcher().removeAudioProcessor(player.getGranulator());
		} catch(NullPointerException e){
			//Le processeur n'a pas encore �t� ajout� dans le processeur. 
		}
		this.player.setGrains(false);
	}
}
