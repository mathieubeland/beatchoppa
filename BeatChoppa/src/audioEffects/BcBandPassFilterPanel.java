package audioEffects;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import syntheziser.BcSynthesizer;
import tarsosDSPModifiedClasses.ModifiedBandPass;
import util.Util;

public class BcBandPassFilterPanel extends BcAudioEffectPanel {
	private BandPassListener listener;
	
	public BcBandPassFilterPanel(ModifiedBandPass processor, BcSynthesizer container) {
		super(processor, container);
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), BcAudioEffectsManager.BandPassInfo.getProcessorName(), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		//Connection de l'�couteur
		listener = new BandPassListener();
		tfParam1.addActionListener(listener);
		tfParam2.addActionListener(listener);
		
		//Initialiser les boites avec les valeurs du processeur
		tfParam1.setText(String.valueOf(((ModifiedBandPass)processor).getFrequence()));
		tfParam2.setText(String.valueOf(((ModifiedBandPass)processor).getBandWidth()));
		
		//Modification des labels par d�faut
		lblParam1.setText("Frequency (Hz) ( > 100): ");
		lblParam2.setText("Band width (Hz) (down):");
	}
	
	private class BandPassListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			if(e.getSource() == tfParam1){ //Frequency
				String param = tfParam1.getText();
				if(Util.sanitizeStringNumericalValue(container, param)){
					((ModifiedBandPass)processor).setFrequency((float) Double.parseDouble(param));;
				} else {
					tfParam1.setText(String.valueOf(((ModifiedBandPass)processor).getFrequence()));
				}
			} else if(e.getSource() == tfParam2){ //Scale
				String param = tfParam2.getText();
				if(Util.sanitizeStringNumericalValue(container, param)){
					((ModifiedBandPass)processor).setBandWidth((float)Double.parseDouble(param));
				} else {
					tfParam2.setText(String.valueOf(((ModifiedBandPass)processor).getBandWidth()));
				}
			}
		}
	}

}