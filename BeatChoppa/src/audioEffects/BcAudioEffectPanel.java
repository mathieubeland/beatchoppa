package audioEffects;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import be.tarsos.dsp.AudioProcessor;
import syntheziser.BcSynthesizer;

import java.awt.Color;
import javax.swing.border.TitledBorder;

import audioPlayer.BcPlayer;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Dimension;
import java.awt.Rectangle;
import javax.swing.JTextField;

public abstract class BcAudioEffectPanel extends JPanel {
	protected AudioProcessor processor;
	protected JPanel panelContent;
	protected JLabel lblParam1;
	protected JTextField tfParam1;
	protected JLabel lblParam2;
	protected JTextField tfParam2;
	protected BcSynthesizer container;
	
	private JPanel panelClose;
	private JLabel lblBuffer;
	private JButton btnX;
	private Listener lst;
	
	public BcAudioEffectPanel(AudioProcessor processor, BcSynthesizer container) {
		this.processor = processor;
		this.container = container;
		
		setBorder(new TitledBorder(null, "Effect", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		panelContent = new JPanel();
		add(panelContent);
		
		lblParam1 = new JLabel("Param1: ");
		panelContent.add(lblParam1);
		
		tfParam1 = new JTextField();
		panelContent.add(tfParam1);
		tfParam1.setColumns(10);
		
		lblParam2 = new JLabel("Param2:");
		panelContent.add(lblParam2);
		
		tfParam2 = new JTextField();
		tfParam2.setColumns(10);
		panelContent.add(tfParam2);
		
		panelClose = new JPanel();
		add(panelClose);
		
		lblBuffer = new JLabel("");
		lblBuffer.setPreferredSize(new Dimension(30, 0));
		lblBuffer.setBounds(new Rectangle(0, 0, 30, 0));
		lblBuffer.setMinimumSize(new Dimension(30, 0));
		panelClose.add(lblBuffer);
		
		btnX = new JButton("X");
		btnX.setForeground(Color.RED);
		panelClose.add(btnX);
		
		lst = new Listener();
		btnX.addActionListener(lst);
	}
	
	public void selfDestruct(){
		container.getEffectsManager().deleteAudioProcessor(this.processor);
		container.getScrollPane().setViewportView(null);
		container.updateSynthesisEffectsList();
		container.selectLasteffect();
	}
	
	private class Listener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			if(e.getSource() == btnX){
				selfDestruct();
			}
		}
	}
}
