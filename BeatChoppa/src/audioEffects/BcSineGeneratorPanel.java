package audioEffects;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import syntheziser.BcSynthesizer;
import tarsosDSPModifiedClasses.ModifiedSineGenerator;
import util.Util;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;

public class BcSineGeneratorPanel extends BcAudioEffectPanel {
	private Listener2 listener;
	
	public BcSineGeneratorPanel(ModifiedSineGenerator processor, BcSynthesizer container) {
		super(processor, container);
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), BcAudioEffectsManager.SineGeneratorInfo.getProcessorName(), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		//Connection de l'�couteur
		listener = new Listener2();
		tfParam1.addActionListener(listener);
		tfParam2.addActionListener(listener);
		
		//Initialiser les boites avec les valeurs du processeur
		tfParam1.setText(String.valueOf(((ModifiedSineGenerator)processor).getFrequency()));
		tfParam2.setText(String.valueOf(((ModifiedSineGenerator)processor).getGain()));
		
		//Modification des label par d�faut
		lblParam1.setText("Frequency (Hz): ");
		lblParam2.setText("Gain (0 =  no alteration): ");
	}
	
	private class Listener2 implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			if(e.getSource() == tfParam1){ //frequence
				String param = tfParam1.getText();
				if(Util.sanitizeStringNumericalValue(container, param)){
					((ModifiedSineGenerator)processor).setFrequency(Double.parseDouble(param));;
				} else {
					tfParam1.setText(String.valueOf(((ModifiedSineGenerator)processor).getFrequency()));
				}
			} else if(e.getSource() == tfParam2){ //gain
				String param = tfParam2.getText();
				if(Util.sanitizeStringBetween1And0(container, param)){
					((ModifiedSineGenerator)processor).setGain(Double.parseDouble(param));
				} else {
					tfParam2.setText(String.valueOf(((ModifiedSineGenerator)processor).getGain()));
				}
			}
		}
	}

}
