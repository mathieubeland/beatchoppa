package audioEffects;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import syntheziser.BcSynthesizer;
import tarsosDSPModifiedClasses.ModifiedDelayEffect;
import util.Util;

public class BcDelayEffectPanel extends BcAudioEffectPanel {
	private DelayEffectListener listener;
	
	public BcDelayEffectPanel(ModifiedDelayEffect processor, BcSynthesizer container) {
		super(processor, container);
		setBorder(new TitledBorder(null, BcAudioEffectsManager.DelayEffectInfo.getProcessorName(), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		//Connection de l'�couteur
		listener = new DelayEffectListener();
		tfParam1.addActionListener(listener);
		tfParam2.addActionListener(listener);
		
		//Initialiser les boites avec les valeurs du processeur
		tfParam1.setText(String.valueOf(((ModifiedDelayEffect)processor).getEchoLength()));
		tfParam2.setText(String.valueOf(((ModifiedDelayEffect)processor).getDecay()));
		
		//Modification des label par d�faut
		lblParam1.setText("Echo lenght (s)(> 0): ");
		lblParam2.setText("Decay (]0, 1]):");
	}
	
	private class DelayEffectListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			if(e.getSource() == tfParam1){ //Echo lenght
				String param = tfParam1.getText();
				if(Util.sanitizeStringNumericalValue(container, param)){
					((ModifiedDelayEffect)processor).setEchoLength(Double.parseDouble(param));;
				} else {
					tfParam1.setText(String.valueOf(((ModifiedDelayEffect)processor).getEchoLength()));
				}
			} else if(e.getSource() == tfParam2){ //Decay
				String param = tfParam2.getText();
				if(Util.sanitizeStringBetween1And0(container, param)){
					((ModifiedDelayEffect)processor).setDecay(Double.parseDouble(param));
				} else {
					tfParam2.setText(String.valueOf(((ModifiedDelayEffect)processor).getDecay()));
				}
			}
		}
	}

}
