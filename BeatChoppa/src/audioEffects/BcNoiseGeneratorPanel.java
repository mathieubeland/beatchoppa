package audioEffects;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import syntheziser.BcSynthesizer;
import tarsosDSPModifiedClasses.ModifiedDelayEffect;
import tarsosDSPModifiedClasses.ModifiedNoiseGenerator;
import util.Util;

public class BcNoiseGeneratorPanel extends BcAudioEffectPanel {
	private NoiseGeneratorListener listener;
	
	public BcNoiseGeneratorPanel(ModifiedNoiseGenerator processor, BcSynthesizer container) {
		super(processor, container);
		setBorder(new TitledBorder(null, BcAudioEffectsManager.NoiseGeneratorInfo.getProcessorName(), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		//Connection de l'�couteur
		listener = new NoiseGeneratorListener();
		tfParam1.addActionListener(listener);;
		
		//Initialiser les boites avec les valeurs du processeur
		tfParam1.setText(String.valueOf(((ModifiedNoiseGenerator)processor).getGain()));
		panelContent.remove(tfParam2);
		
		//Modification des label par d�faut
		lblParam1.setText("Gain: ");
		panelContent.remove(lblParam2);
	}
	
	private class NoiseGeneratorListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			if(e.getSource() == tfParam1){ //Echo lenght
				String param = tfParam1.getText();
				if(Util.sanitizeStringNumericalValue(container, param)){
					((ModifiedNoiseGenerator)processor).setGain(Double.parseDouble(param));;
				} else {
					tfParam1.setText(String.valueOf(((ModifiedNoiseGenerator)processor).getGain()));
				}
			}
		}
	}

}
