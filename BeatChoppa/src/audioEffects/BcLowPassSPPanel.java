package audioEffects;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import syntheziser.BcSynthesizer;
import tarsosDSPModifiedClasses.ModifiedLowPassSP;
import util.Util;
import javax.swing.border.TitledBorder;

import javax.swing.UIManager;
import java.awt.Color;

public class BcLowPassSPPanel extends BcAudioEffectPanel  {
	private Listener2 listener;
	
	public BcLowPassSPPanel(ModifiedLowPassSP processor, BcSynthesizer container) {
		super(processor, container);
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), BcAudioEffectsManager.LowPassSPInfo.getProcessorName(), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		//Connection de l'�couteur
		listener = new Listener2();
		tfParam1.addActionListener(listener);
		
		//Initialiser les boites avec les valeurs du processeur
		//tfParam1.setText(String.valueOf(container.get));
		
		//Modification des label par d�faut
		tfParam1.setText(String.valueOf(((ModifiedLowPassSP)processor).getFrequence()));
		panelContent.remove(tfParam2);
		
		//Modification des label par d�faut
		lblParam1.setText("Frequency (Hz): ");
		panelContent.remove(lblParam2);
	}
	
	private class Listener2 implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			if(e.getSource() == tfParam1){ //Frequency
				String param = tfParam1.getText();
				if(Util.sanitizeStringNumericalValue(container, param)){
					((ModifiedLowPassSP)processor).setFrequency(Float.parseFloat(param));;
				} else {
					tfParam1.setText(String.valueOf(((ModifiedLowPassSP)processor).getFrequence()));
				}
			}
		}
	}
}
