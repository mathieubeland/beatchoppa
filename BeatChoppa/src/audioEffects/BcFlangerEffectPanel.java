package audioEffects;

import util.Util;

import javax.swing.JTextField;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.border.TitledBorder;

import syntheziser.BcSynthesizer;
import tarsosDSPModifiedClasses.ModifiedFlangerEffect;

import javax.swing.UIManager;
import java.awt.Color;

public class BcFlangerEffectPanel extends BcAudioEffectPanel {
	private JTextField tfParam3;
	private Listener2 listener;
	private JLabel lblParam3;
	
	public BcFlangerEffectPanel(ModifiedFlangerEffect processor, BcSynthesizer container) {
		super(processor, container);
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), BcAudioEffectsManager.FlangerEffectInfo.getProcessorName(), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		this.processor = processor;
		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		lblParam3 = new JLabel("Frequency (Hz): ");
		panelContent.add(lblParam3);
		tfParam3 = new JTextField();
		panelContent.add(tfParam3);
		tfParam3.setColumns(10);
		
		//Connection de l'�couteur
		listener = new Listener2();
		tfParam1.addActionListener(listener);
		tfParam2.addActionListener(listener);
		tfParam3.addActionListener(listener);
		
		//Initialiser les boites avec les valeurs du processeur
		tfParam1.setText(String.valueOf(((ModifiedFlangerEffect)processor).getFlangerLenght()));
		tfParam2.setText(String.valueOf(((ModifiedFlangerEffect)processor).getWet()));
		tfParam3.setText(String.valueOf(((ModifiedFlangerEffect)processor).getLfoFrequency()));
		
		//Modification des label par d�faut
		lblParam1.setText("Lenght (s)(>0)(Works best < 1.0): ");
		lblParam2.setText("Wetness [0, 1]: ");
	}
	
	private class Listener2 implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			if(e.getSource() == tfParam1){
				String param = tfParam1.getText();
				if(Util.sanitizeStringNumericalValue(container, param)){ //lenght
					((ModifiedFlangerEffect)processor).setFlangerLength(Double.parseDouble(param));
				} else {
					tfParam1.setText(String.valueOf(((ModifiedFlangerEffect)processor).getFlangerLenght()));
				}
			} else if(e.getSource() == tfParam2){
				String param = tfParam2.getText();
				if(Util.sanitizeStringBetween1And0(container, param)){ //wetness
					((ModifiedFlangerEffect)processor).setWet(Double.parseDouble(param));
				} else {
					tfParam2.setText(String.valueOf(((ModifiedFlangerEffect)processor).getWet()));
				}
			} else if(e.getSource() == tfParam3){
				String param = tfParam3.getText();
				if(Util.sanitizeStringNumericalValue(container, param)){ //LFOFrequency
					((ModifiedFlangerEffect)processor).setLFOFrequency(Double.parseDouble(param));
				} else {
					tfParam3.setText(String.valueOf(((ModifiedFlangerEffect)processor).getLfoFrequency()));
				}
			}
		}
	}
}