package audioEffects;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import audioPlayer.BcAudioDispatcher;
import syntheziser.BcSynthesizer;
import tarsosDSPModifiedClasses.ModifiedWaveformSimilarityBasedOverlapAdd;
import util.Util;
import javax.swing.border.TitledBorder;

import javax.swing.UIManager;
import java.awt.Color;

public class BcWaveformSimilarityBasedOverlapAddPanel extends BcAudioEffectPanel  {
	private WSOLAListener listener;
	
	public BcWaveformSimilarityBasedOverlapAddPanel(ModifiedWaveformSimilarityBasedOverlapAdd processor, BcSynthesizer container) {
		super(processor, container);
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), BcAudioEffectsManager.WaveformSimilarityBasedOverlapAddInfo.getProcessorName(), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		//Connection de l'�couteur
		listener = new WSOLAListener();
		tfParam1.addActionListener(listener);
		
		//Modification des label par d�faut
		tfParam1.setText(String.valueOf(container.getEffectsManager().getTempo()));
		panelContent.remove(tfParam2);
		
		//Modification des label par d�faut
		lblParam1.setText("Frequency (Hz): ");
		panelContent.remove(lblParam2);
	}
	
	@Override
	public void selfDestruct(){
		//On ne veut pas d�truire le WSOLA du Player, seulement l'enlever du synt�tiseur
		container.getEffectsManager().deleteDummieWSOLA();
		container.getScrollPane().setViewportView(null);
		container.updateSynthesisEffectsList();
		container.selectLasteffect();
	}
	
	private class WSOLAListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			if(e.getSource() == tfParam1){
				String param = tfParam1.getText();
				if(Util.sanitizeStringNumericalValue(container, param)){
					container.getEffectsManager().setTempo(Double.parseDouble(param));;
				} else {
					tfParam1.setText(String.valueOf(container.getEffectsManager().getTempo()));
				}
			}
		}
	}
}
