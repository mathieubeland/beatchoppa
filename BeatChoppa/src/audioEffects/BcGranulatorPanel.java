package audioEffects;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import audioPlayer.BcGranulatorWrapper;
import syntheziser.BcSynthesizer;
import tarsosDSPModifiedClasses.ModifiedOptimizedGranulator;
import javax.swing.JButton;

/**
 * 
 * @author Mathieu
 * Cette classe est tr�s fortemetn inspir�e de la classe GranulatorExample faisant partie du package de d�monstration de la librairie TarsosDSP.
 */

public class BcGranulatorPanel extends BcAudioEffectPanel {
	private BcGranulatorWrapper wrapper;
	
	private final JSlider timeStretchSlider = new JSlider(0, 3000);
	private final JSlider pitchShiftSlider = new JSlider(0, 3000);
	private final JSlider grainSizeSlider = new JSlider(1, 300);
	private final JSlider grainIntervallSlider = new JSlider(1, 300);
	private final JSlider grainRandomnesslSlider = new JSlider(0, 1000);
	private final JSlider positionSlider = new JSlider(0, 20000);
	private final JButton btnRestore = new JButton("Restore Default Value");
	
	private JLabel timeStretchLabel;
	private JLabel pitchShiftLabel;
	private JLabel grainSizeLabel;
	private JLabel grainIntervalLabel;
	private JLabel grainRandomnessLabel;
	private JLabel positionLabel;
	
	public BcGranulatorPanel(ModifiedOptimizedGranulator processor, BcSynthesizer container) {
		super(processor, container);
		setBorder(new TitledBorder(null, BcAudioEffectsManager.GranulatorInfo.getProcessorName(), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		wrapper = container.getPlayer().getGranulatorWrapper();
	
		setLayout(new GridLayout(0, 2));
		timeStretchLabel = new JLabel("Time stretch factor (%): " + (wrapper.getTimeStretchFactor() * 1000.0f));
		pitchShiftLabel = new JLabel("Pitch shift factor (%): " + (wrapper.getPitchFactor() * 1000.0f));
		grainSizeLabel = new JLabel("Grain size (ms): " + wrapper.getGrainSize());
		grainIntervalLabel = new JLabel("Grain interval (ms): " + wrapper.getGrainInterval());
		grainRandomnessLabel = new JLabel("Grain randomness (%): " + (wrapper.getGrainRandomness() * 1000.0f));
		positionLabel = new JLabel("Position (s): " + (wrapper.getPosition() * 1000.0f));
		
		//Initialiser les boites avec les valeurs du processeur
		panelContent.remove(tfParam1);
		panelContent.remove(tfParam2);
		
		//Modification des label par d�faut
		panelContent.remove(lblParam1);
		panelContent.remove(lblParam2);
		
		panelContent.add(btnRestore);
		btnRestore.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				wrapper.restoreDefault();
				timeStretchLabel = new JLabel("Time stretch factor (%): " + (wrapper.getTimeStretchFactor() * 1000.0f));
				pitchShiftLabel = new JLabel("Pitch shift factor (%): " + (wrapper.getPitchFactor() * 1000.0f));
				grainSizeLabel = new JLabel("Grain size (ms): " + wrapper.getGrainSize());
				grainIntervalLabel = new JLabel("Grain interval (ms): " + wrapper.getGrainInterval());
				grainRandomnessLabel = new JLabel("Grain randomness (%): " + (wrapper.getGrainRandomness() * 1000.0f));
				positionLabel = new JLabel("Position (s): " + (wrapper.getPosition() * 1000.0f));
				timeStretchSlider.setValue((int)(wrapper.getTimeStretchFactor() * 1000.0f));
				pitchShiftSlider.setValue((int)(wrapper.getPitchFactor() * 1000.0f));
				positionSlider.setValue((int)(wrapper.getPosition()));
				grainRandomnesslSlider.setValue((int)(wrapper.getGrainRandomness() * 1000.0f));
				grainSizeSlider.setValue((int) wrapper.getGrainSize());
				grainIntervallSlider.setValue((int)wrapper.getGrainInterval());
			}
		});
		
		timeStretchSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				float currentFactor = (float) timeStretchSlider.getValue() / 1000.0f;
				timeStretchLabel.setText("Time stretch factor (%): " + String.format(
						"%.1f", currentFactor * 100));
				if(wrapper.getGranulator()!=null)
					wrapper.setTimeStretchFactor(currentFactor);
			}
		});

		pitchShiftSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				float currentFactor = (float) pitchShiftSlider.getValue() / 1000.0f;
				pitchShiftLabel.setText("Pitch shift factor (%): " + String.format("%.1f", currentFactor * 100));
				if(wrapper.getGranulator()!=null)
					wrapper.setPitchFactor(currentFactor);
			}
		});
		
		grainIntervallSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				int grainInterval = grainIntervallSlider.getValue();
				grainIntervalLabel.setText(String.format(
						"Grain interval (ms): %d", grainInterval));
				if(wrapper.getGranulator()!=null)
					wrapper.setGrainInterval(grainInterval);
			}
		});
		
		grainSizeSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				int grainSize = grainSizeSlider.getValue();
				grainSizeLabel.setText(String.format(
						"Grain size (ms): %d", grainSize));
				if(wrapper.getGranulator()!=null)
					wrapper.setGrainSize(grainSize);
			}
		});
		
		grainRandomnesslSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				float grainRandomness = (float) grainRandomnesslSlider.getValue() / 1000.0f;
				grainRandomnessLabel.setText(String.format(
						"Grain randomness (%%): %.1f", grainRandomness * 100));
				if(wrapper.getGranulator()!=null)
					wrapper.setGrainRandomness(grainRandomness);
			}
		});
		
		positionSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				float position = (float) positionSlider.getValue() / 1000.0f;
				positionLabel.setText(String.format("Position (s): %.3f", position));
				if(wrapper.getGranulator()!=null)
					wrapper.setPosition(position);
			}
		});
		
		timeStretchSlider.setValue((int)(wrapper.getTimeStretchFactor() * 1000.0f));
		pitchShiftSlider.setValue((int)(wrapper.getPitchFactor() * 1000.0f));
		positionSlider.setValue((int)(wrapper.getPosition()));
		grainRandomnesslSlider.setValue((int)(wrapper.getGrainRandomness() * 1000.0f));
		grainSizeSlider.setValue((int) wrapper.getGrainSize());
		grainIntervallSlider.setValue((int)wrapper.getGrainInterval());
		this.add(timeStretchLabel);
		this.add(timeStretchSlider);
		this.add(pitchShiftLabel);
		this.add(pitchShiftSlider);
		this.add(grainIntervalLabel);
		this.add(grainIntervallSlider);
		this.add(grainSizeLabel);
		this.add(grainSizeSlider);
		this.add(grainRandomnessLabel);
		this.add(grainRandomnesslSlider);
		this.add(positionLabel);
		this.add(positionSlider);
	}
	
	@Override
	public void selfDestruct(){
		//On ne veut pas d�truire le WSOLA du Player, seulement l'enlever du synt�tiseur
		container.getEffectsManager().deleteDummieGranulator();
		container.getScrollPane().setViewportView(null);
		container.updateSynthesisEffectsList();
		container.selectLasteffect();
	}

}