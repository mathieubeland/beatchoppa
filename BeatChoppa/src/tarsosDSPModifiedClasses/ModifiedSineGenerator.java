package tarsosDSPModifiedClasses;

import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;

public class ModifiedSineGenerator implements AudioProcessor{
	
	protected double gain;
	protected double frequency;
	protected double phase;
	
	public ModifiedSineGenerator(){
		this(0.1,75);
	}
	
	public ModifiedSineGenerator(double gain,double frequency){
		this.gain = gain;
		this.frequency = frequency;
		this.phase = 0;
	}

	@Override
	public boolean process(AudioEvent audioEvent) {
		float[] buffer = audioEvent.getFloatBuffer();
		double sampleRate = audioEvent.getSampleRate();
		double twoPiF = 2 * Math.PI * frequency;
		double time = 0;
		for(int i = 0 ; i < buffer.length ; i++){
			time = i / sampleRate;
			buffer[i] += (float) (gain * Math.sin(twoPiF * time + phase));
		}
		phase = twoPiF * buffer.length / sampleRate + phase;
		return true;
	}
	
	

	public double getGain() {
		return gain;
	}

	public void setGain(double gain) {
		this.gain = gain;
	}

	public double getFrequency() {
		return frequency;
	}

	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}

	@Override
	public void processingFinished() {
	}
}