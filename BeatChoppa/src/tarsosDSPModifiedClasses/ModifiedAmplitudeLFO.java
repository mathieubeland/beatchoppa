/*
*      _______                       _____   _____ _____  
*     |__   __|                     |  __ \ / ____|  __ \ 
*        | | __ _ _ __ ___  ___  ___| |  | | (___ | |__) |
*        | |/ _` | '__/ __|/ _ \/ __| |  | |\___ \|  ___/ 
*        | | (_| | |  \__ \ (_) \__ \ |__| |____) | |     
*        |_|\__,_|_|  |___/\___/|___/_____/|_____/|_|     
*                                                         
* -------------------------------------------------------------
*
* TarsosDSP is developed by Joren Six at IPEM, University Ghent
*  
* -------------------------------------------------------------
*
*  Info: http://0110.be/tag/TarsosDSP
*  Github: https://github.com/JorenSix/TarsosDSP
*  Releases: http://0110.be/releases/TarsosDSP/
*  
*  TarsosDSP includes modified source code by various authors,
*  for credits and info, see README.
* 
*/

package tarsosDSPModifiedClasses;

import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;

public class ModifiedAmplitudeLFO implements AudioProcessor {
	
	private double frequency;
	private double scaleParameter;
	private double phase;
	
	public ModifiedAmplitudeLFO(){
		this(1.5,0.75);
	}
	
	public ModifiedAmplitudeLFO(double frequency, double scaleParameter){
		this.frequency = frequency;
		this.scaleParameter = scaleParameter;
		phase = 0;
	}
	

	@Override
	public boolean process(AudioEvent audioEvent) {
		float[] buffer = audioEvent.getFloatBuffer();
		double sampleRate = audioEvent.getSampleRate();
		double twoPiF = 2 * Math.PI * frequency;
		double time = 0;
		for(int i = 0 ; i < buffer.length ; i++){
			time = i / sampleRate;
			float gain =  (float) (scaleParameter * Math.sin(twoPiF * time + phase));
			buffer[i] = gain * buffer[i];
		}
		phase = twoPiF * buffer.length / sampleRate + phase;
		return true;
	}
	
	@Override
	public void processingFinished() {
	}

	//Ajout� ************************************************

	public double getFrequency() {
		return frequency;
	}

	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}

	public double getScaleParameter() {
		return scaleParameter;
	}

	public void setScaleParameter(double scaleParameter) {
		this.scaleParameter = scaleParameter;
	}
	// *******************************************************
}
