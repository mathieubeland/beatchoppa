package util;

import java.awt.Component;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;

public class Util {
	
	public static final DecimalFormat df2 = new DecimalFormat( "#00" );
	public static final DecimalFormat df3 = new DecimalFormat( "#000" );
	public static String milisecondsToString(double seconds){
	    int mins = (int) seconds/ 60;
	    double remainder = seconds - mins * 60;
	    int secs = (int) Math.floor(remainder);
	    remainder = remainder - secs;
	    int  milis = (int) (remainder * 1000.0);
	    return df2.format(mins) + ":" + df2.format(secs) + ":" + df3.format(milis);
	}
	
	public static String secondsToString(double seconds){
	    int mins = (int) seconds/ 60;
	    double remainder = seconds - mins * 60;
	    int secs = (int) Math.floor(remainder);
	    return df2.format(mins) + ":" + df2.format(secs);	
	}
	
	public static boolean sanitizeStringNumericalValue(Component from, String value){
		double numeric = 0.0;
		try{
			numeric = Double.parseDouble(value);
		} catch (NumberFormatException e){
			JOptionPane.showMessageDialog(from, "The string passed is not a number.");
			return false;
		}  catch (NullPointerException e){
			JOptionPane.showMessageDialog(from, "The string passed is empty.");
			return false;
		}
		return true;
	}
	
	public static boolean sanitizeStringOfSeconds(Component from, String time){
		if(!sanitizeStringNumericalValue(from, time)){
			return false;
		}
		double numeric = Double.parseDouble(time);
		if(numeric < 0){
			JOptionPane.showMessageDialog(from, "Can not enter a negative time value.");
			return false;
		}
		return true;
	}
	
	public static boolean sanitizeStringBetween1And0(Component from, String value){
		if(!sanitizeStringNumericalValue(from, value)){
			return false;
		}
		double numeric = Double.parseDouble(value);
		if(numeric < 0 || numeric > 1.0){
			JOptionPane.showMessageDialog(from, "The value must be between 0.0 and 1.0.");
			return false;
		}
		return true;
	}
	
	public static boolean sanitizeStartOrEndInSamples(Component from, String sanitizee, String value, long start, long end, long songLenght){
		if(Util.sanitizeStringNumericalValue(from, value)){
			boolean result = true;
			long numericalValue = 0;
			try {
				numericalValue = Long.parseLong(value);
			} catch(NumberFormatException e) {
				JOptionPane.showMessageDialog(from, "The value must be an integer.");
				return false;
			}
			if(numericalValue < 0){
				JOptionPane.showMessageDialog(from, "Can not enter a negative time value.");
				result = false;
			} else if(sanitizee.equals("start")){
				if(numericalValue > end){
					result = false;
					JOptionPane.showMessageDialog(from, "The beginning must be before the end.");
				}
			} else if(sanitizee.equals("end")){
				if(numericalValue < start){
					result = false;
					JOptionPane.showMessageDialog(from, "The end must be after the beginning.");
				}
				if(numericalValue > songLenght){
					result = false;
					JOptionPane.showMessageDialog(from, "The end of the sample can not be after the end of the file.");
				}
			}
			return result;
		} else {
			return false;
		}
	}
	
	public static boolean sanitizeStartOrEndInSeconds(Component from, String sanitizee, String value, double start, double end, double songLenght){
		if(Util.sanitizeStringOfSeconds(from, value)){
			boolean result = true;
			double numericalValue = Double.parseDouble(value);
			if(sanitizee.equals("start")){
				if(numericalValue > end){
					result = false;
					JOptionPane.showMessageDialog(from, "The beginning must be before the end.");
				}
			} else if(sanitizee.equals("end")){
				if(numericalValue < start){
					result = false;
					JOptionPane.showMessageDialog(from, "The end must be after the beginning.");
				}
				if(numericalValue > songLenght){
					result = false;
					JOptionPane.showMessageDialog(from, "The end of the sample can not be after the end of the file.");
				}
			}
			return result;
		} else {
			return false;
		}
	}
}
