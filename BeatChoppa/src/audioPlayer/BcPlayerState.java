//Cette �num�ration a �t� prise du module de d�monstration de TarsosDSP et a �t� l�g�rement modifi�e

package audioPlayer;
	public enum BcPlayerState{
		/**
		 * The file is playing
		 */
		PLAYING,
		/**
		 * Audio play back is paused.
		 */
		PAUZED,		
		/**
		 * Audio play back is stopped. 
		 * �a arrive quand l'extrait arrive � la fin. On recommence donc l'extrait au d�but. 
		 */
		STOPPED
}