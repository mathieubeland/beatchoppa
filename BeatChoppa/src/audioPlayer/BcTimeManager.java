package audioPlayer;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;

/*
 * Cette classe permet de g�rer le temps du AudioPlayer
 * Elle g�re �galement les conversions entre le temps en secondes et les samples. 
 * */

public class BcTimeManager implements AudioProcessor{
	private double durationInSeconds;
	private long lenghtInSamples;
	private long pausedAtInSamples;
	private double pausedAtInSeconds;
	private long startInSamples;
	private double startInSeconds;
	private long endInSamples;
	private double endInSeconds;
	private boolean loop;
	private BcPlayer player;

	public BcTimeManager(BcPlayer player, boolean loop) {
		this.player = player;
		this.lenghtInSamples = player.getFileFormat().getFrameLength();
		this.durationInSeconds = lenghtInSamples / player.getAudioFormat().getFrameRate();
		this.pausedAtInSamples = 0;
		this.pausedAtInSeconds = 0;
		this.startInSamples = 0;
		this.startInSeconds = 0;
		this.endInSamples = lenghtInSamples;
		this.endInSeconds = durationInSeconds;
		this.player = player;
		this.loop = loop;
	}

	//M�thodes li�es � la gestion du temps
	public long getStartInSamples() {
		return startInSamples;
	}

	public boolean setStartInSamples(long start) {
		boolean result;
		if(this.endInSamples > start){
			this.startInSamples = start;
			this.startInSeconds = startInSamples / player.getAudioFormat().getFrameRate();
			result =  true;
		} else {
			result = false;
		}
		player.updateElementsRelatedToTime();
		return result;
	}

	public double getStartInSeconds() {
		return startInSeconds;
	}

	public boolean setStartInSeconds(double start) {
		boolean result;
		if(this.endInSeconds > start){
			this.startInSeconds = start;
			this.startInSamples = (long) (startInSeconds * player.getAudioFormat().getFrameRate());
			result = true;
		} else {
			result = false;
		}
		player.updateElementsRelatedToTime();
		return result;
	}

	public long getEndInSamples() {
		return endInSamples;
	}

	public boolean setEndInSamples(long end) {
		boolean result;
		if(this.startInSamples < end){
			endInSamples = end;
			this.endInSeconds = endInSamples / player.getAudioFormat().getFrameRate();
			result = true;
		} else {
			result = false;
		}
		player.updateElementsRelatedToTime();
		return result;
	}

	public double getEndInSeconds() {
		return endInSeconds;
	}
	
	public boolean setEndInSeconds(double end) {
		boolean result;
		if(this.startInSeconds < end){
			this.endInSeconds = end;
			this.endInSamples = (long) (endInSeconds * player.getAudioFormat().getFrameRate());
			result = true;
		} else {
			result = false;
		}
		player.updateElementsRelatedToTime();
		return result;
	}

	public long getResumeAtInSamples() {
		return pausedAtInSamples;
	}
	
	public void setPausedAtInSamples(long pausedAtInSamples) {
		this.pausedAtInSamples = pausedAtInSamples;
		this.pausedAtInSeconds = pausedAtInSamples / player.getAudioFormat().getFrameRate();
	}

	public void setPausedAtInSeconds(double pausedAtInSeconds) {
		this.pausedAtInSeconds = pausedAtInSeconds;
		this.pausedAtInSamples = (long) (pausedAtInSeconds * player.getAudioFormat().getFrameRate());
	}

	public double getResumeAtInSeconds() {
		return pausedAtInSeconds;
	}

	public double getDurationInSeconds() {
		return durationInSeconds;
	}
	
	public long getLenghtInSamples() {
		return lenghtInSamples;
	}
	
	public double getCurrentTimeInSeconds() {
		try {
			return player.getDispatcher().secondsProcessed();
		} catch (NullPointerException e) {
			return 0.0;
		}
	}
	
	public double getCurrentTimeInSamples() {
		try {
			return player.getDispatcher().getSamplesProcessed();
		} catch (NullPointerException e) {
			return 0.0;
		}
	}

	@Override
	public void processingFinished() {
		if(loop && player.getState() == BcPlayerState.STOPPED){
			player.play();
		}
	}

	@Override
	public boolean process(AudioEvent audioEvent) {
		//On s'assure que seul l'extrait s�lectionn� puisse �tre jou�. 
		if(audioEvent.getSamplesProcessed() < this.startInSamples){
			player.play(this.startInSamples);
			return false;
		}else if(audioEvent.getSamplesProcessed() > this.endInSamples){
			player.play(this.startInSamples);
			return false;
		}
		return true;
	}
	
	public boolean isLooping(){
		return this.loop;
	}
	
	public void setLooping(boolean loop){
		this.loop = loop;
	}
}
