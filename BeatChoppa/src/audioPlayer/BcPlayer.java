package audioPlayer;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.GainProcessor;
import be.tarsos.dsp.WaveformSimilarityBasedOverlapAdd.Parameters;
import be.tarsos.dsp.io.jvm.AudioPlayer;
import be.tarsos.dsp.io.jvm.WaveformWriter;
import syntheziser.BcSynthesizer;
import tarsosDSPModifiedClasses.ModifiedOptimizedGranulator;
import tarsosDSPModifiedClasses.ModifiedWaveformSimilarityBasedOverlapAdd;
import audioPlayer.BcAudioDispatcher;

/**
 * Cette classe permet de g�rer la lecture des fichiers audio. 
 * C'est �galement elle qui g�re tout ce qui a � voir avec la la gestion des processeurs
 * qui traitent le signal audio et le processeur qui permet d'enregistrer l'extrait s�lectionn�. 
 */

public class BcPlayer{
	
	private PropertyChangeSupport support = new PropertyChangeSupport(this);
	private GainProcessor gainProcessor;
	private AudioPlayer audioPlayer;
	private BcAudioDispatcher dispatcher;
	private File loadedFile;
	private double gain; //Permet de contr�ler le volume
	private WaveformWriter writer;
	private AudioFileFormat fileFormat;
	private AudioFormat audioFormat;
	private BcPlayerState state;
	private double maximumGain;
	private Thread mainThread;
	private BcTimeManager time;
	private double sampleRate;
	private boolean timeStretch;
	private double tempo;
	private ModifiedWaveformSimilarityBasedOverlapAdd wsola;
	private boolean grains;
	private BcGranulatorWrapper granulator;
	protected List<AudioProcessor> audioProcessors;
	final private int bufferSize = 1024;
	private BcSynthesizer synthesizer;
	private File savedFile;
	
	public BcPlayer(BcSynthesizer synthesizer, File file){
		this.synthesizer = synthesizer;
		state = BcPlayerState.PAUZED;
		load(file);
		gain = 1.0;
		tempo = 1.0;
		timeStretch = false;
		grains = false;
		granulator = new BcGranulatorWrapper();
		audioProcessors = new CopyOnWriteArrayList<AudioProcessor>();
		maximumGain = 2.0; //Une valeur de plus de 2 produit un bruit consid�rable.
		gainProcessor = new GainProcessor(gain);
	}
	
	public void load(File file) {
		loadedFile = file;
		try {
			fileFormat = AudioSystem.getAudioFileFormat(loadedFile);
			audioFormat = fileFormat.getFormat();
			sampleRate = AudioSystem.getAudioInputStream(loadedFile).getFormat().getSampleRate();
		} catch (UnsupportedAudioFileException e) {
			throw new Error(e);
		} catch (IOException e) {
			throw new Error(e);
		}
		setState(BcPlayerState.PAUZED);
		//Certaine chose ne peuvent �tre initialis�e qu'en fonction du fichier s�lectionn�. 
		time = new BcTimeManager(BcPlayer.this, false);
	}
	
	/*
	 * Cette m�thode g�re la lecture du fichier audio charg�. C'est ici que l'on ajoute tous les processeurs
	 * au dispatcher (incluant ceux permettant de faire jouer la musique ou d'enregistrer l'extrait.
	 */
	
	public void initialise(boolean createWAV){
		try {
			//On peut ajouter autant de processors qu'on le veut pour faire des effets sur le son.
			if(dispatcher != null && !dispatcher.isStopped()){
				dispatcher.stop(); //Il faut arr�ter le dispatcher si on veut que la lecture se termine et que le garbage collector
								   //supprime �ventuellement le dispatcher qui ro�le sur le thread. 
			}
			
			//Le WaveformSimilarityBasedOverlapAdd permet de faire tu time stretching et est diff�rent des autres processeurs. 
			//On ne peut pas en mettre deux et on doit lui passer le dispatcher parce qu'il modifie le sampleRate. 
			if(timeStretch){
				wsola = new ModifiedWaveformSimilarityBasedOverlapAdd(Parameters.slowdownDefaults(tempo,audioFormat.getSampleRate()));
				dispatcher = BcAudioDispatcherFactory.fromFile(loadedFile, wsola.getInputBufferSize(), wsola.getOverlap(), this);
				wsola.setDispatcher(dispatcher);
				dispatcher.addAudioProcessor(wsola);
				if(grains){
					dispatcher.addAudioProcessor(granulator.createGranulator(audioFormat.getSampleRate(), wsola.getInputBufferSize()));
				}
			} else {
				dispatcher = BcAudioDispatcherFactory.fromFile(loadedFile, bufferSize, bufferSize/2, this);
				if(grains){
					dispatcher.addAudioProcessor(granulator.createGranulator(audioFormat.getSampleRate(), bufferSize));
				}
			}
			dispatcher.addAudioProcessor(gainProcessor);
			dispatcher.addAudioProcessor(time);
			
			//On ajoute les processeurs que l'utilisateur peut ajouter lui-m�me
			for(AudioProcessor effect:this.audioProcessors){
				dispatcher.addAudioProcessor(effect);
			}
			
			//Il faut toujours mettre l'AudioPlayer ou le WaveFormWriter � la fin pour que les modifications s'appliquent lors de la lecture ou de l'�criture du nouveau fichier.
			if(createWAV){
				//On doit lire du d�but � la fin sans loop. On cr�e donc un nouveau loop processor qui ne va pas influencer le cours du loopProcessor principal.
				writer = new WaveformWriter(audioFormat, this.savedFile.toString());
				dispatcher.addAudioProcessor(writer);
				time.setLooping(false);
			}else{
				//On doit �galement cr�er un nouvel audioPlayer � chaque initialisation.
				//Il est TR�S important de passer en param�tre le longeur du buffer, car c'est ce qui permet de bien synchroniser des �v�nements et le son.
				if(timeStretch){
					audioPlayer = new AudioPlayer(audioFormat, wsola.getInputBufferSize());
				} else {
					audioPlayer = new AudioPlayer(audioFormat, bufferSize);
				}
				dispatcher.addAudioProcessor(audioPlayer);
			}
			dispatcher.skip(time.getResumeAtInSamples());
			//On doit passer le nouveau dispatcher au TimeTracker pour qu'il puisse g�rer le temps. 
			mainThread = new Thread(dispatcher,"Audio Player Thread");
			mainThread.start();
			setState(BcPlayerState.PLAYING);
		} catch (UnsupportedAudioFileException e) {
			throw new Error(e);
		} catch (IOException e) {
			throw new Error(e);
		}  catch (LineUnavailableException e) {
			throw new Error(e);
		}
	}
	
	public void eject(){
		loadedFile = null;
		try{
			stop();
		} catch(IllegalStateException e){
			//Can not stop when nothing is playing: Tant mieux
		}
	}
	
	public void stop(){
		this.setLooping(false);
		if(dispatcher != null){
			dispatcher.stop();
		}
	}
	
	public void pause() {
		if(state == BcPlayerState.PLAYING){
			setState(BcPlayerState.PAUZED);
			time.setPausedAtInSamples(dispatcher.getSamplesProcessed());
			dispatcher.stop();
		} else {
			throw new IllegalStateException("Can not pause when nothing is playing");
		}
	}
	
	public void play(double StartTime){
		time.setPausedAtInSeconds(StartTime);
		initialise(false);
	}
	
	public void play(long samples){
		time.setPausedAtInSamples(samples);
		initialise(false);
	}
	
	public void play(){
		if(state == BcPlayerState.STOPPED){
			time.setPausedAtInSamples(time.getStartInSamples());
			initialise(false);
		} else if(state == BcPlayerState.PAUZED){
			initialise(false);
		} else if(state == BcPlayerState.PLAYING){
			throw new IllegalStateException("Player is already playing.");
		}
	}

	/*Permet de sauvegarder l'extrait s�lectionn�*/
	public void record(File savedFile){
		this.savedFile = savedFile;
		initialise(true);
	}
	
	/*Permet d'ajouter un processeur � la liste de processeurs audio*/
	public void addAudioProcessor(AudioProcessor audioProcessor) {
		if(state == BcPlayerState.PLAYING){ 
			pause();
			this.audioProcessors.add(audioProcessor);
			play();
		} else {
			this.audioProcessors.add(audioProcessor);
		}
	}

	public BcGranulatorWrapper getGranulatorWrapper(){
		return granulator;
	}
	
	public ModifiedOptimizedGranulator getGranulator() {
		return granulator.getGranulator();
	}

	public ModifiedWaveformSimilarityBasedOverlapAdd getWsola() {
		return wsola;
	}

	public void setWsola(ModifiedWaveformSimilarityBasedOverlapAdd wsola) {
		this.wsola = wsola;
	}

	public boolean isTimeStretch() {
		return timeStretch;
	}

	public void setTimeStretch(boolean timeStretch) {
		this.timeStretch = timeStretch;
	}

	public double getSampleRate() {
		return sampleRate;
	}
	
	public boolean isGrains() {
		return grains;
	}

	public void setGrains(boolean grains) {
		this.grains = grains;
	}

	public void setSampleRate(double sampleRate) {
		this.sampleRate = sampleRate;
	}
	
	public AudioProcessor getProcessorByIndex(int index){
		return audioProcessors.get(index);
	}
	
	public int getNumberOfProcessors(){
		return audioProcessors.size();
	}
	
	public double getTempo() {
		return tempo;
	}

	public void setTempo(double tempo) {
		if(state == BcPlayerState.PLAYING){ 
			pause();
			this.tempo = tempo;
			play();
		} else {
			this.tempo = tempo;
		}
	}

	public void deleteAudioProcessor(AudioProcessor audioProcessors) {
		this.audioProcessors.remove(audioProcessors);
	}
	
	public List<AudioProcessor> getAudioProcessors() {
		return this.audioProcessors;
	}

	public BcAudioDispatcher getDispatcher() {
		return dispatcher;
	}

	public void setGain(double newGain){
		gain = newGain;
		gainProcessor.setGain(gain);
	}
	
	public double getMaximumGain() {
		return maximumGain;
	}
	
	public BcTimeManager getTimeManager(){
		return time;
	}
	
	public boolean isLooping(){
		return time.isLooping();
	}
	
	public void setLooping(boolean loop){
		time.setLooping(loop);
	}

	public void setState(BcPlayerState newState){
		BcPlayerState oldState = state;
		state = newState;
		support.firePropertyChange("state", oldState, newState);
	}
	
	public BcPlayerState getState() {
		return state;
	}

	public AudioFormat getAudioFormat() {
		return audioFormat;
	}

	public AudioFileFormat getFileFormat() {
		return fileFormat;
	}

	public void addPropertyChangeListener(PropertyChangeListener l) {
		support.addPropertyChangeListener(l);
	}

	public void removePropertyChangeListener(PropertyChangeListener l) {
		support.removePropertyChangeListener(l);
	}
	
	public void updateElementsRelatedToTime(){
		this.synthesizer.updateElementsRelatedToTime();
	}
	
	public String getFileName(){
		String infos = "File: " + loadedFile.getName();
		return infos;
	}
	
	public int getBufferSize() {
		return bufferSize;
	}

	public String getFormat(){
		String chanelType = "MONO";
		if(audioFormat.getChannels() != 1){
			chanelType = "STEREO";
		}
		String infos = "Format: " + fileFormat.getType() + " " + audioFormat.getFrameRate() + " " + chanelType + "\n";
		return infos;
	}
}
