package audioPlayer;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.io.TarsosDSPAudioInputStream;
import tarsosDSPModifiedClasses.ModifiedAudioDispatcher;

/**
 * La classe h�rite d'AudioDispatcher qui a �t� l�g�rement modifi�e pour que l'on puisse utiliser 
 * les attriuts de la classes (private chang� pour protected). On a �galement modifi� la fonction 
 * run pour pouvoir indiquer que la chanson s'est termin�e et pour d'arr�ter le flux au nombre exact de bytes. 
 * Pour pouvoir lire le nombre exact de samples.
 */

public class BcAudioDispatcher extends ModifiedAudioDispatcher  {

	private  BcPlayer player;
	//Cette variable nous permet de garder l'esprit du Dispatcher. Elle indique seulement si nous nous sommes rendus � la fin
	//de l'extrait d�fini. Le boolean stopped permet encore de faire la distinction entre un arr�t caus� par la fonction stopped, donc quand on l'arr�tre explicitement
	// et quand le stream s'arr�te, car il a atteint la fin. 
	private boolean stoppedBeforeEnd = false;
	
	public BcAudioDispatcher(TarsosDSPAudioInputStream stream, int audioBufferSize, int bufferOverlap, BcPlayer player) {
		super(stream, audioBufferSize, bufferOverlap);
		this.player = player;
	}
	
	public void skip(long frames){
		bytesToSkip = frames * format.getFrameSize(); 
	}

	public long getSamplesProcessed(){
		return Math.round(bytesProcessed/format.getFrameSize());
	}
	
	public List<AudioProcessor> getProcessorsList(){
		return audioProcessors;
	}
	
	public void stop() {
		stopped = true;
		for (final AudioProcessor processor : audioProcessors) {
			processor.processingFinished();
		}
		try {
			audioInputStream.close();
		} catch (IOException e) {
			LOG.log(Level.SEVERE, "Closing audio stream error.", e);
		}
	}
	
	public void deleteAudioProcessor(final AudioProcessor audioProcessor) {
		audioProcessors.remove(audioProcessor);
	}
	
	public void run() {
		
		int bytesRead = 0;
		
		if(bytesToSkip!=0){
			skipToStart();
		}
	
		//Read the first (and in some cases last) audio block.
		try {
			//needed to get correct time info when skipping first x seconds
			audioEvent.setBytesProcessed(bytesProcessed);
			bytesRead = readNextAudioBlock();
		} catch (IOException e) {
			String message="Error while reading audio input stream: " + e.getMessage();	
			LOG.warning(message);
			throw new Error(message);
		}

		// As long as the stream has not ended
		while (bytesRead != 0 && !stopped && !stoppedBeforeEnd) {
			//Makes sure the right buffers are processed, they can be changed by audio processors.
			for (final AudioProcessor processor : audioProcessors) {
				if(!processor.process(audioEvent)){
					//skip to the next audio processors if false is returned.
					break;
				}
			}
			
			if(!stopped && !stoppedBeforeEnd){
				//Les �l�ments ajout�s permettent d'arr�ter le flux au nombre exact de bytes. Pour pouvoir lire le nombre exact de samples.
				long end = player.getTimeManager().getEndInSamples()*format.getFrameSize();
				long previous = bytesProcessed;
				
				//Update the number of bytes processed;
				bytesProcessed += bytesRead;
				audioEvent.setBytesProcessed(bytesProcessed);
				
				long difference =  bytesProcessed - previous;
				long bytesLeft = end - bytesProcessed;
				if(bytesLeft <= difference && bytesLeft > 0){
					player.setState(BcPlayerState.STOPPED);
					stoppedBeforeEnd = true;
					setStepSizeAndOverlap((int)bytesLeft/2, 0);
				}
					
				// Read, convert and process consecutive overlapping buffers.
				// Slide the buffer.
				if(bytesLeft <= 0){
					player.setState(BcPlayerState.STOPPED);
					stoppedBeforeEnd = true;
					bytesProcessed += ((end - player.getTimeManager().getStartInSamples()*format.getFrameSize()) - bytesRead);
				} else {
					try {
						bytesRead = readNextAudioBlock();
						audioEvent.setOverlap(floatOverlap);
					} catch (IOException e) {
						String message="Error while reading audio input stream: " + e.getMessage();	
						LOG.warning(message);
						throw new Error(message);
					}
					if(stoppedBeforeEnd){
						bytesProcessed += bytesRead;
					}
				}
			}
		}
		
		player.updateElementsRelatedToTime();
		// Notify all processors that no more data is available. 
		// when stop() is called processingFinished is called explicitly, no need to do this again.
		// The explicit call is to prevent timing issues.
		if(!stopped){
			stop();
		}
	}
	
}
