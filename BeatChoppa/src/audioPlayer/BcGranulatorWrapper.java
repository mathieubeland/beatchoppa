package audioPlayer;

import tarsosDSPModifiedClasses.ModifiedOptimizedGranulator;

public class BcGranulatorWrapper {
	private ModifiedOptimizedGranulator granulator;
	final private float defaulttimeStretchFactor = 1.0f;
	final private float defaultPitchFactor = 1.0f;
	final private float defaultGrainInterval = 40.0f;
	final private float defaultGrainSize = 100.0f;
	final private float defaultGrainRandomness = 0.0f;
	final private double defaultPosition = 0.0f;
	
	
	private float timeStretchFactor = defaulttimeStretchFactor;
	private float pitchFactor = defaultPitchFactor;
	private float grainInterval = defaultGrainInterval;
	private float grainSize = defaultGrainSize;
	private float grainRandomness = defaultGrainRandomness;
	private double position = defaultPosition;
	
	public BcGranulatorWrapper(){};
	
	public float getTimeStretchFactor() {
		return timeStretchFactor;
	}
	public void setTimeStretchFactor(float timeStretchFactor) {
		if(granulator != null){
			granulator.setTimeStretchFactor(timeStretchFactor);
		}
		this.timeStretchFactor = timeStretchFactor;
	}
	public float getPitchFactor() {
		return pitchFactor;
	}
	public void setPitchFactor(float pitchFactor) {
		if(granulator != null){
			granulator.setPitchShiftFactor(pitchFactor);
		}
		this.pitchFactor = pitchFactor;
	}
	public float getGrainInterval() {
		return grainInterval;
	}
	public void setGrainInterval(float grainInterval) {
		if(granulator != null){
			granulator.setGrainInterval((int)grainInterval);
		}
		this.grainInterval = grainInterval;
	}
	public float getGrainSize() {
		return grainSize;
	}
	public void setGrainSize(float grainSize) {
		if(granulator != null){
			granulator.setGrainSize((int)grainSize);
		}
		this.grainSize = grainSize;
	}
	public float getGrainRandomness() {
		return grainRandomness;
	}
	public void setGrainRandomness(float grainRandomness) {
		if(granulator != null){
			granulator.setGrainRandomness(grainRandomness);
		}
		this.grainRandomness = grainRandomness;
	}
	
	public ModifiedOptimizedGranulator getGranulator(){
		return granulator;
	}
	
	public double getPosition() {
		return position;
	}

	public void setPosition(double position) {
		this.position = position;
	}

	public void restoreDefault(){
		if(granulator != null){
			timeStretchFactor = defaulttimeStretchFactor;
			pitchFactor = defaultPitchFactor;
			grainInterval = defaultGrainInterval;
			grainSize = defaultGrainSize;
			grainRandomness = defaultGrainRandomness;
			position = defaultPosition;
			
			granulator.setGrainInterval((int)this.grainInterval);
			granulator.setTimeStretchFactor(this.timeStretchFactor);
			granulator.setPitchShiftFactor(this.pitchFactor);
			granulator.setPosition((int)this.position);
			granulator.setGrainRandomness(this.grainRandomness);
		}
	}
	
	public ModifiedOptimizedGranulator createGranulator(float sampleRate,int bufferSize){
		this.granulator = new ModifiedOptimizedGranulator(sampleRate, bufferSize);
		granulator.setGrainInterval((int)this.grainInterval);
		granulator.setTimeStretchFactor(this.timeStretchFactor);
		granulator.setPitchShiftFactor(this.pitchFactor);
		granulator.setPosition((int)this.position);
		granulator.setGrainRandomness(this.grainRandomness);
		return this.granulator;
	}
	
}
