package sineWavePanel;

import java.awt.Color;

/**
 * Cette classe dessiner le panneau interractif repr�sentant l'onde sinuso�dale.
 */


import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import audioPlayer.BcPlayer;

class BcInterractiveSineWavePanel extends JPanel
{
	private float[] samplesArray;
	private float[] samplesDisplayed;
	private float [] levelTreeFourArray;
	private float maximumAmplitude;
	private float middleOfCanvas; //Le milieux du canevas
	private int nbSamplesRepresentedOnScreen; //Le nombre de samples qui sont th�oriquement repr�sent�s � l'�cran
	private int centerOfZoom; //Le point � partir duquel la SineWave est zoom�e
	private int totalnbSamples;
	private int zoomLevel;
	private int lastZoomLevel; //Permet de savoir quel �tait le niveau de zoom ant�rieur pour ne pas r�p�ter inutilement certains calculs. 
	private LevelsOfRepresentation representation = LevelsOfRepresentation.ONE;
	private int minimumDisplayedSamples = 30; //Le nombre minimum de samples pouvant �tre repr�sent�, quand on zoom au maximum
	final private int circleDiametre = 5;
	private int circleShift;
	private boolean redrawSineWave = true;
	final private int mimimumDistanceBetweenCircles = 2;
	private int maximumSamplesRepresentedWithCircles;
	final private int nbPointsPerPixelsOfWidth = 2; //Le nombre de de points repr�sent�s pour un pixel de largeur du panel. 
	private int nbPointsDisplayed;
	private BcScope scope;
	final private int phaseTwoSampleForPixelThreshold = 6;
	private BcPlayer player;
	private PanelListener listener;
	
	public BcInterractiveSineWavePanel(int x, int y, int width, int height, BcPlayer player, float[] samplesArray, BcScope scope) {
		super();
		super.setBounds(x, y, width, height);
		setBackground(new Color(35, 35, 35));
		
		this.player = player;
		this.scope = scope;
		this.samplesArray = samplesArray;
		totalnbSamples = samplesArray.length;
		maximumAmplitude = 0;
		for(int i = 0; i < totalnbSamples; ++i){
			if(maximumAmplitude < Math.abs(samplesArray[i])){
				maximumAmplitude = Math.abs(samplesArray[i]);
			}
		}
		this.zoomLevel = 0;
		this.lastZoomLevel = -1;
		middleOfCanvas = this.getHeight()/2;
	
		//Permet de dessiner le cercle exactement autour du points d�pendemment si le diam�tre est par ou impair. 
		if(circleDiametre % 2 == 0){
			circleShift = circleDiametre/2;
		} else {
			circleShift = (circleDiametre - 1)/2;
		}
	
		nbPointsDisplayed = this.getWidth()*nbPointsPerPixelsOfWidth;
		maximumSamplesRepresentedWithCircles =  (this.getWidth() - mimimumDistanceBetweenCircles) / (mimimumDistanceBetweenCircles + circleDiametre);
		
		listener = new PanelListener();
		addMouseWheelListener(listener);
		addMouseListener(listener);
		
		isZoomDifferent(0,0);
		recalibrateZoom(0,0);
		getPointsToDisplay();
		repaint();
	}

	@Override
	protected void paintComponent (Graphics g)
	{
		super.paintComponent(g);
		((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		//Dans tous les cas, on dessine une ligne � la ligne 0;
		g.setColor(new Color(0, 255, 0, 180));
		((Graphics2D)g).drawLine(0, (int)middleOfCanvas, getWidth(), (int)middleOfCanvas);
		
		if(redrawSineWave){
			if(representation == LevelsOfRepresentation.ONE || representation == LevelsOfRepresentation.TWO){
				int opacity = 255;
				//On diminue l'opacit� des bandes rendu au niveau deux. 
				if(representation == LevelsOfRepresentation.ONE){
					g.setColor(Color.GREEN);
				} else if(representation == LevelsOfRepresentation.TWO){
					int total =  getWidth() * (int) phaseTwoSampleForPixelThreshold;
					opacity = (int)(255*nbSamplesRepresentedOnScreen/total);
					g.setColor(new Color(0, 255, 0, opacity));
				}
				
				for(int i = 0; i < this.getWidth()-nbPointsPerPixelsOfWidth; ++i){
					//La multiplication n�gative sert � inverser l'axe pour qu'il soit du bon bord
					float first = (float) (samplesDisplayed[i * nbPointsPerPixelsOfWidth] * -middleOfCanvas) + middleOfCanvas; 
					float second = (float) (samplesDisplayed[i * nbPointsPerPixelsOfWidth + 1] * -middleOfCanvas) + middleOfCanvas;
					((Graphics2D)g).drawLine(i, (int)first, i, (int)second);
				}
				
				//On dessine une ligne pour faciliter la transition entre le niveau 2 et 3.
				if(representation == LevelsOfRepresentation.TWO){
					opacity = 255 - opacity;
					g.setColor(new Color(0, 100, 255, opacity));
					float[] line = new float[samplesDisplayed.length/2];
					int j = 0;
					for(int i = 0; i < this.getWidth()-nbPointsPerPixelsOfWidth; ++i, ++j){
						if(Math.abs(samplesDisplayed[i * nbPointsPerPixelsOfWidth]) > Math.abs(samplesDisplayed[i * nbPointsPerPixelsOfWidth + 1])){
							line[j] = (float) (samplesDisplayed[i * nbPointsPerPixelsOfWidth] * -middleOfCanvas) + middleOfCanvas; 
						} else {
							line[j] = (float) (samplesDisplayed[i * nbPointsPerPixelsOfWidth + 1] * -middleOfCanvas) + middleOfCanvas;
						}
					}
					for(int i = 0; i < line.length - 1; ++i){
						((Graphics2D)g).drawLine(i, (int)line[i], i+1, (int)line[i+1]);
					}
				}
			} else if(representation == LevelsOfRepresentation.THREE || representation == LevelsOfRepresentation.FOUR){
				boolean cursorVisible = scope.isCursorVisible();
				boolean startVisible = scope.isStartVisible();
				boolean endVisible = scope.isEndVisible();
				int cursorId = scope.getCursorId();
				int startId = scope.getStartId();
				int endiD = scope.getEndId();
				g.setColor(new Color(0, 100, 255));
				for(int i = 0; i < samplesDisplayed.length - 1; ++i){
					float first = (float) (samplesDisplayed[i] * -middleOfCanvas) + middleOfCanvas; 
					float second = (float) (samplesDisplayed[i + 1] * -middleOfCanvas) + middleOfCanvas;
					double position1 = getPositionOnPanel(((double)i)+ scope.getStart());
					double position2 = getPositionOnPanel(((double)i + 1.0)+ scope.getStart());
					((Graphics2D)g).drawLine((int)position1, (int)first, (int) (position2), (int)second);
					if(cursorVisible && i == cursorId){
						scope.setCursorDisplayedPosition((int)position1);
					}
					if(startVisible && i == startId){
						scope.setStartDisplayedPosition((int)position1);
					}
					if(endVisible && i == endiD){
						scope.setEndDisplayedPosition((int)position1);
					}
					//On dessine des cercles pour repr�senter les samples, en plus de la ligne. 
					if(representation == LevelsOfRepresentation.FOUR){
						((Graphics2D)g).drawOval((int)position1 - circleShift, (int)first - circleShift, circleDiametre, circleDiametre);
					}
				}
			}
		}
		
		//On dessine l'extrait s�lectionn� (d�but et fin) s'il est dans l'interval repr�sent�
		long startScope = (long)scope.getStart();
		long endScope  = (long)scope.getEnd();
		long startMusicSample = player.getTimeManager().getStartInSamples();
		long endMusicSample  = player.getTimeManager().getEndInSamples();
		//On veut d'abord savoir si l'intersection des deux intervals est nul. 
		if(((endScope < startMusicSample) && (startScope > endMusicSample)) || ((endScope > startMusicSample) && (startScope < endMusicSample))){
			//Si ce n'est pas le cas, on veut afficher l'intersection sur le Panel.
			double start;
			double end;
			
			if(startMusicSample < startScope){
				start = 0;
			} else {
				if(representation == LevelsOfRepresentation.FOUR || representation == LevelsOfRepresentation.THREE){
					start = scope.getStartDisplayedPosition();
				} else  {
					start = getPositionOnPanel((int)player.getTimeManager().getStartInSamples());
				}
			}
			if(endScope <= endMusicSample){
				end = getWidth();
			} else {
				if(representation == LevelsOfRepresentation.FOUR || representation == LevelsOfRepresentation.THREE){
					end = scope.getEndDisplayedPosition();
				} else  {
					end = getPositionOnPanel((int)player.getTimeManager().getEndInSamples());
				}
			}
			g.setColor(new Color(255,140,0,25));
			((Graphics2D)g).fillRect((int)start, 0, (int)(end-start), this.getHeight() - 1);
			g.setColor(new Color(255,165,0, 200));
			((Graphics2D)g).drawLine((int)start, 0, (int)start, getHeight()-3);
			((Graphics2D)g).drawLine((int)end, 0, (int)end, getHeight()-3);
		}
		
		g.setColor(Color.RED);
		if(scope.isKeepWithCursor() && scope.isDrawCursorInMiddle()){
			//On dessiner la barre au milieu
			((Graphics2D)g).drawLine((int)getWidth()/2, 0, (int)getWidth()/2, getHeight()-3);
		} else {
			//On dessine la barre de temps si elle est visible dans l'interval repr�sent�
			if(scope.isCursorVisible()){
				double timeBar;
				if(representation== LevelsOfRepresentation.FOUR || representation == LevelsOfRepresentation.THREE){
					timeBar = scope.getCursorDisplayedPosition();
				} else  {
					timeBar = getPositionOnPanel(scope.getSongCursorPosition());
				}
				((Graphics2D)g).drawLine((int)timeBar, 0, (int)timeBar, getHeight()-3);
			}
		}
	}
	
	protected double getPositionOnPanel(double position){
		double relativePosition = position - scope.getStart();
		double x = (relativePosition / scope.getWidth()) * this.getWidth();
		return x;
	}
	
	/**
	 * 	Savoir si le zoom est diff�rent (donc si on recalibre le zoom (recalibrateZoom)).
	 * @param zoom
	 * @param posX
	 * @return
	 */
	public boolean isZoomDifferent(int zoom, int posX){
		//On change le niveau de pr�cision du zoom
		boolean minimumIsDisplayed = false;
		int initialnbSamplesRepresentedOnScreen = nbSamplesRepresentedOnScreen;
		
		//Le nombre de samples th�oriquement repr�sent�s � l'�cran d�pend du nombre de pixels de largeur
		//de la surface o� l'on repr�sente le signal et du nombre de samples que l'on veut repr�senter pour chaque pixel.
		if(zoomLevel + zoom >= 0){
			//On ne peut pas d�zoomer plus que quand tout l'extrait est repr�sent�
			zoomLevel = 0;
			nbSamplesRepresentedOnScreen =  totalnbSamples;
		} else {
			zoomLevel += zoom;
			nbSamplesRepresentedOnScreen = (int) (totalnbSamples / (Math.pow(0.9, zoomLevel)));	
			
			//On ne peut pas zoomer plus que ce qui va affich� moins que  le nombre de samples minimum
			if(nbSamplesRepresentedOnScreen <= minimumDisplayedSamples){
				zoomLevel -= zoom;
				nbSamplesRepresentedOnScreen = initialnbSamplesRepresentedOnScreen;
			}
		}
		return zoomLevel != lastZoomLevel;
	}
	
	/**
	 * A UTILISER SEULEMENT SI ON SAIT QUE LE ZOOM A CHANG� (isZoomDifferent)
	 * @param zoom
	 * @param posX
	 */
	
	public void recalibrateZoom(int zoom, int posX){
		//Voir l'�num�ration "LevelsOfRepresentation"
		if(nbSamplesRepresentedOnScreen >= phaseTwoSampleForPixelThreshold * this.getWidth() ){ //ONE, nbPointsDisplayed = this.getWidth()*nbPointsPerPixelsOfWidth;
			representation = LevelsOfRepresentation.ONE;
		} else {
			if(nbSamplesRepresentedOnScreen >= this.getWidth()){ //TWO
				representation = LevelsOfRepresentation.TWO;
			} else {
				//On ne peut pas repr�senter les samples avec des cercles, s'ils sont trop coll�s ou s'ils sont 
				//dessin�s les uns par dessus les autres. 
				if(nbSamplesRepresentedOnScreen > maximumSamplesRepresentedWithCircles){
					representation = LevelsOfRepresentation.THREE;
				} else {
					if(nbSamplesRepresentedOnScreen <= minimumDisplayedSamples){
						nbSamplesRepresentedOnScreen = minimumDisplayedSamples;
					}
					representation = LevelsOfRepresentation.FOUR;
				}
			}
		}
		
		//Apr�s avoir trouv� le nombre de samples repr�sent�s, il faut d�terminer quel sera l'interval repr�sent�. 
		//Tout passe par la fonction Scope qui fait le point enter la barre et le panel (this).
		double focus = (double) posX/this.getWidth();
		scope.calculateNewScope(nbSamplesRepresentedOnScreen, focus);
	}
	
	/**
	 * Appeler cette fonction avant repaint pour avoir la niveau sineWave. 
	 * Si cette fonction n'est pas appel�e, elle ne sera pas redessin�e. 
	 * Si le zoom a chang� (isZoomDifferent), on appelle recalibrateZoom avant, sinon le tableau
	 *  calcul� n'aura pas les bonnes dimensions.
	 */
	
	public void getPointsToDisplay(){
		//D�pendamment du niveau de repr�sentation, la mani�re dont on choisit les samples qui seront repr�sent�s varie. 
		if(representation == LevelsOfRepresentation.ONE || representation == LevelsOfRepresentation.TWO){
			samplesDisplayed = new float[nbPointsDisplayed];

			//Pour chacun des intervals, on va chercher les extremums (minimum et maximum)
			double startingPoint = scope.getStart(); 
			int highest = 0;
			int lowest = 0;
			float highestValue = 0;
			float lowestValue = 0;

			double interval = (double)nbSamplesRepresentedOnScreen / (double) this.getWidth();
			for(int i = 0; i < this.getWidth(); ++i){
				highest = 0;
				lowest = 0;
				highestValue = 0;
				lowestValue = 0;
				
				for(int j = (int)startingPoint; j < startingPoint + interval && j < samplesArray.length; ++j){
					if(samplesArray[j] > highestValue){
						highestValue = samplesArray[j];
						highest = j;
					}
					if(samplesArray[j] < lowestValue){
						lowestValue = samplesArray[j];
						lowest = j;
					}
				}
				samplesDisplayed[i * nbPointsPerPixelsOfWidth] = lowestValue;
				samplesDisplayed[i * nbPointsPerPixelsOfWidth + 1] = highestValue;
				startingPoint += interval;
			}
		} else if(representation == LevelsOfRepresentation.THREE || representation == LevelsOfRepresentation.FOUR){
			//On ajoute un sample pour ne pas qu'il y ait un sample en plus lorsque l'on dessine le sinewave
			samplesDisplayed = new float[nbSamplesRepresentedOnScreen + 1];
			boolean scopeVisible = scope.isCursorVisible();
			boolean startVisible = scope.isStartVisible();
			boolean endVisible = scope.isEndVisible();
			int cursorPosition = scope.getSongCursorPosition();
			int startPosition = scope.getSongStartPosition();
			int endPosition = scope.getSongEndPosition();
			int j = (int)scope.getStart();
			for(int i = 0; i < nbSamplesRepresentedOnScreen + 1 && j < scope.getEnd() && j < scope.getSamplesLenght(); ++i, ++j){
				samplesDisplayed[i] = samplesArray[j];
				if(scopeVisible && j == cursorPosition){
					scope.setCursorId(i);
				}
				if(startVisible && j == startPosition){
					scope.setStartId(i);
				}
				if(endVisible && j== endPosition){
					scope.setEndId(i);
				}
			}
			j++;
			samplesDisplayed[nbSamplesRepresentedOnScreen] = samplesArray[j];
		}
		//On prend en note le dernier niveau de zoom pour pouvoir le comparer la prochaine fois.
		lastZoomLevel = zoomLevel;
		//Il va falloir redessiner le sineWave pour avoir la photo. 
		redrawSineWave = true;
	}
	
	public float[] getSampleArray() {
		return samplesArray;
	}
	
	public void keepWithCursor(boolean keep){
		scope.setKeepWithCursor(keep);
	}
	
	public void updateTime(){
		if(scope.updateTime()){
			getPointsToDisplay();
		}
		repaint();
	}
	
	private long getTimeInSampleRelativeToClick(int posX){
		double newTime = ((double)posX / (double)getWidth() * scope.getWidth()) + scope.getStart();
		return (long)newTime;
	}
	
	private void setStart(int posX){
		player.getTimeManager().setStartInSamples((long)getTimeInSampleRelativeToClick(posX));
	}
	
	private void setEnd(int posX){
		player.getTimeManager().setEndInSamples((long)getTimeInSampleRelativeToClick(posX));
	}
	
	public void redraw(){
		getPointsToDisplay();
		repaint();
	}
	
	public enum LevelsOfRepresentation{
		//Il y a plusieurs niveaux de repr�sentation diff�rent des samples � l'�cran.
		
		//1. Au d�but, on va aller chercher les deux valeurs maximales (plus bas et plus haut) pour 1 bit repr�sent�. 
		ONE,
		//2. Nous faisons une transition entre les phases 1 et 2 quand il reste moins de 4 samples pour chaque pixel;
		TWO,
		//3. Finalement, quand il n'y aura plus assez de samples pour que chaque pixel en repr�sente un, nous allons prendre tout le reste.
		THREE,
		//4. Lorsq'il y aura assez d'espace pour repr�senter un cercle de "circleRadius" pour chaque sample, un cercle est dessin� autour de chaque sample. 
		FOUR
	}
	
	private class PanelListener implements MouseWheelListener, MouseListener {
		private boolean leftDown = false;
		private boolean rightDown = false;
		private boolean scrollButtonDown = false;
		
		@Override
		public void mouseWheelMoved(MouseWheelEvent e) {
			//Avant de refaire tout le processus pour rien, on v�rifie si le niveau zoom est diff�rent.
			//S'il ne l'est pas, on ne refait pas tout le processus pour rien. 
			if(isZoomDifferent(e.getWheelRotation(), e.getX())){
				if(isZoomDifferent(e.getWheelRotation(), e.getX())){
					recalibrateZoom(e.getWheelRotation(), e.getX());
					getPointsToDisplay();
					repaint();
				}
			}
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
		    if (e.getButton() == MouseEvent.BUTTON1) {
		    	leftDown = true;
		    }
		    if (e.getButton() == MouseEvent.BUTTON3) {
		    	rightDown = true;
		    }
		    if (e.getButton() == MouseEvent.BUTTON2) {
		    	scrollButtonDown = true;
		    }		
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		    if (leftDown) {
		    	setStart(e.getX());
		    	leftDown = false;
		    }
		    if (rightDown) {
		    	setEnd(e.getX());
		    	rightDown = false;
		    }
		    if (scrollButtonDown) {
				player.play(getTimeInSampleRelativeToClick(e.getX()));
				updateTime();
		    	scrollButtonDown = false;
		    }
		}
	}
}
