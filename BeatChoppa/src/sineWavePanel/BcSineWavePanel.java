package sineWavePanel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.EtchedBorder;

/**
 * 
 * @author Mathieu
 * Cette classe va seulement permettre de dessiner la repr�sentation du singal sinuso�dal, mais ne permettra pas d'autres interractions. 
 */

public class BcSineWavePanel extends JPanel {
	public BcSineWavePanel() {
		setBorder(null);
	}
	private float[] samplesArray;
	private float[] samplesDisplayed;
	private float middleOfCanvas; //Le milieux du canevas
	private int totalnbSamples;
	private int nbPointsDisplayed;
	private int intervalLength = 0;
	final private int nbPointsPerPixelsOfWidth = 2;

	public void initializePanel(float[] sampleArray) {
		setBackground(new Color(35, 35, 35));
		this.samplesArray = sampleArray;
		totalnbSamples = sampleArray.length;
		nbPointsDisplayed = this.getWidth() * nbPointsPerPixelsOfWidth;
		middleOfCanvas = this.getHeight()/2;
		this.getPointsToDisplay();
	}

	protected void paintComponent (Graphics g)
	{
		super.paintComponent(g);
		((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		g.setColor(Color.GREEN);
		//On dessine une bande � chaque pixel du panel dont les deux points sont les extremums de l'interval repr�sent�. 
		for(int i = 0; i < this.getWidth()-nbPointsPerPixelsOfWidth; ++i){
			//La multiplication n�gative sert � inverser l'axe pour qu'il soit du bon bord
			float first = (float) (samplesDisplayed[i * nbPointsPerPixelsOfWidth] * -middleOfCanvas) + middleOfCanvas; 
			float second = (float) (samplesDisplayed[i * nbPointsPerPixelsOfWidth + 1] * -middleOfCanvas) + middleOfCanvas;
			((Graphics2D)g).drawLine(i, (int)first, i+1, (int)second);
		}
	}
	
	public void getPointsToDisplay(){
		samplesDisplayed = new float[nbPointsDisplayed];

		intervalLength = totalnbSamples / this.getWidth();
		int startingPoint = 0; 
		int highest = 0;
		int lowest = 0;
		float highestValue = (float) 0;
		float lowestValue = (float) 0;
		
		//Pour la meilleure repr�sentation possible, on recherche les extremums des intervals repr�sent�s. 
		for(int i = 0; i < this.getWidth(); ++i){
			highest = 0;
			lowest = 0;
			highestValue = (float) -1.0;
			lowestValue = (float) 1.0;
			for(int j = startingPoint; j < startingPoint+intervalLength; ++j){
				if(samplesArray[j] > highestValue){
					highestValue = samplesArray[j];
					highest = j;
				}
				if(samplesArray[j] < lowestValue){
					lowestValue = samplesArray[j];
					lowest = j;
				}
			}
			samplesDisplayed[i * nbPointsPerPixelsOfWidth] = lowestValue;
			samplesDisplayed[i * nbPointsPerPixelsOfWidth + 1] = highestValue;
			startingPoint += intervalLength;
		}
	}

	public int getIntervalLength() {
		return intervalLength;
	}
}
