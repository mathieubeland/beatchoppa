package sineWavePanel;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import audioPlayer.BcPlayer;
import be.tarsos.dsp.io.TarsosDSPAudioFloatConverter;
import be.tarsos.dsp.io.TarsosDSPAudioFormat;
import be.tarsos.dsp.io.TarsosDSPAudioInputStream;
import be.tarsos.dsp.io.jvm.JVMAudioInputStream;

public class BcSineWaveInterface extends JPanel {
	private float[] samplesArray;
	private BcInterractiveSineWavePanel panel;
	private BcSineWaveBar bar;
	private BcPlayer player;
	private BcScope scope;
	
	public BcSineWaveInterface(int x, int y, int width, int height, BcPlayer player, File file) {
		this.player = player;
		super.setBounds(x, y, width, height);
		setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		
		samplesArray = loadFloatArrayToRAM(file);
		int barHeight = 40;
		int wavePanelHeight = getHeight() - barHeight;
		setLayout(null);
		
		scope = new BcScope(samplesArray.length, this.player);
		
		panel = new BcInterractiveSineWavePanel(0, 0, this.getWidth(), wavePanelHeight, player, samplesArray, scope);
		add(panel);
		
		bar = new BcSineWaveBar(0, wavePanelHeight, this.getWidth(), barHeight, player, samplesArray, scope, this);
		add(bar);
	}
	
	public void updateTime(){
		bar.updateTime();
		panel.updateTime();
	}
	
	public void keepWithCursor(boolean keep){
		panel.keepWithCursor(keep);
	}
	
	public void redraw() {
		panel.redraw();
	}
	
	private float[] loadFloatArrayToRAM(File file) {
		float[] samplesArray = null;
		try{
			AudioFileFormat fileFormat = AudioSystem.getAudioFileFormat(file);
			AudioFormat format = fileFormat.getFormat();
			AudioInputStream stream = AudioSystem.getAudioInputStream(file);
			TarsosDSPAudioInputStream audioStream = new JVMAudioInputStream(stream);
			int numberOfSamples = (int)stream.getFrameLength();
			//Il va falloir le nombre de Samples d�sir� et un stream pour lire les data
			//On va passer d'un byte buffer � un gros float array
			samplesArray = new float[numberOfSamples];
			
			//Il faut maintenant get le byte array � partir du file
			byte[] audioByteBuffer = new byte[(int) file.length()];
			FileInputStream fileInputStream = new FileInputStream(file);
			fileInputStream.read(audioByteBuffer);
			
			//On va maintenant chercher les float as is � partir du float array
			TarsosDSPAudioFormat TarsosFormat = audioStream.getFormat();
			TarsosDSPAudioFloatConverter converter = TarsosDSPAudioFloatConverter.getConverter(TarsosFormat);
			
			int offset = audioByteBuffer.length - (samplesArray.length *2);
			converter.toFloatArray(audioByteBuffer, offset, samplesArray, numberOfSamples);
		} catch(UnsupportedAudioFileException e){
			e.printStackTrace();
		} catch(IOException e){ 
			e.printStackTrace();
		}
		return samplesArray;
	}
}
