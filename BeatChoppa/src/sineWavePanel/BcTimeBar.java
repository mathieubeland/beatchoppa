package sineWavePanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import audioPlayer.BcPlayer;

import javax.swing.border.EtchedBorder;

/**
 * @author Mathieu
 *  Par soucis de performance, la barre du BcInterractiveSineWavePanel sera dessin�e � l'aide d'un BcWaveFormPanel statique.
 *  Cette classe dessinera toute la partie interractive de la barre. 
 */

public class BcTimeBar extends JPanel {
	private int intervalLenght;
	private double x;
	private int totalNumberSamples;
	private BufferedImage sineWave;
	private BcPlayer player;
	private BarListener listener;
	private BcScope scope;
	private double scopeStart;
	private double scopeEnd;
	private BcSineWaveInterface sineInterface;
	
	public BcTimeBar(int x, int y, int width, int height, BcPlayer player, float[] samplesArray, BcScope scope, BcSineWaveInterface sineInterface) {
		super();
		super.setBounds(x, y, width, height);
		setLayout(null);
		this.player = player;
		BcSineWavePanel panel = new BcSineWavePanel();
		panel.setBounds(0, 0, getWidth(), getHeight());
		panel.initializePanel(samplesArray);
		add(panel);
		
		//Pour la performance, on prend une photo du panel. Ainsi, tous les calculs ne seront pas � refaire. 
	    Dimension size = ((JPanel)this).getSize();
	    sineWave = new BufferedImage( size.width, 	size.height , BufferedImage.TYPE_INT_RGB);
	    Graphics2D g2 = sineWave.createGraphics();
	    ((JPanel)this).paint(g2);
	    remove(panel);
	    
	    this.scope = scope;
	    this.sineInterface = sineInterface;
	    totalNumberSamples = samplesArray.length;
	    intervalLenght = totalNumberSamples / getWidth();
	    
	    listener = new BarListener();
	    addMouseListener(listener);
	    addMouseMotionListener(listener);
	}

	@Override
	protected void paintComponent (Graphics g){
		super.paintComponent(g);
		((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.drawImage(sineWave, 0,0,getWidth(),getHeight(), null);
		g.setColor(Color.RED);
		((Graphics2D)g).drawLine((int)x, 0, (int)x, getHeight());
		
		//Dessiner le d�but et la fin
		double startMusicSample = ((double)player.getTimeManager().getStartInSamples() / (double)player.getTimeManager().getLenghtInSamples()) * this.getWidth();
		double endMusicSample = ((double)player.getTimeManager().getEndInSamples() / (double)player.getTimeManager().getLenghtInSamples()) * this.getWidth();
		g.setColor(new Color(255,140,0, 75));
		((Graphics2D)g).fillRect((int)startMusicSample, 0, (int)(endMusicSample - startMusicSample), this.getHeight() - 1);
		g.setColor(new Color(255,140, 0));
		((Graphics2D)g).drawRect((int)startMusicSample, 0, (int)(endMusicSample - startMusicSample), this.getHeight() - 1);
		
		//Dessiner le scope
		scopeStart = calculateScopeStart();
		scopeEnd = calculateScopeEnd();
		g.setColor(new Color(0, 255, 255, 60));
		((Graphics2D)g).fillRect((int)scopeStart, 0, (int)(scopeEnd - scopeStart), this.getHeight() - 1);
		g.setColor(new Color(0, 139,139));
		((Graphics2D)g).drawRect((int)scopeStart, 0, (int)(scopeEnd - scopeStart), this.getHeight() - 1);
	}

	public void updateTime() {
		double sample = player.getTimeManager().getCurrentTimeInSamples();
		int nbSamples = intervalLenght;
		for(x = 0;  sample > nbSamples; ++x){
			nbSamples += intervalLenght;
		}
		repaint();
	}
	
	private double calculateScopeStart(){
		return ((double)scope.getStart() / (double)scope.getSamplesLenght()) * this.getWidth();
	}
	
	private double calculateScopeEnd(){
		return ((double)scope.getEnd() / (double)scope.getSamplesLenght()) * this.getWidth();
	}
	
	private class BarListener implements MouseListener, MouseMotionListener{
		private boolean leftDown = false;
		private boolean rightDown = false;
		private boolean scrollButtonDown = false;
		private int lastPosition;
	
		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
		    if (e.getButton() == MouseEvent.BUTTON1) {
				if(e.getX() >= (int)scopeStart && e.getX() <= (int)scopeEnd){
			    	leftDown = true;
			    	lastPosition = e.getX();
				}
		    }	
		    if (e.getButton() == MouseEvent.BUTTON3) {
		    	rightDown = true;
		    }		
		    if (e.getButton() == MouseEvent.BUTTON2) {
				scrollButtonDown = true;
		    }		
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		    if (scrollButtonDown) {
		    	int positionOnBar = e.getX();
				double newPosition = (double)positionOnBar / (double)getWidth() * player.getTimeManager().getDurationInSeconds();
				player.play(newPosition);
			    scrollButtonDown = false;
				updateTime();
		    }
		    leftDown = false;
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			if(leftDown){
				double difference  = e.getX() - lastPosition;
				double relativeTranslation = difference / (double) getWidth();
				scope.translate(relativeTranslation);
				lastPosition = e.getX();
				sineInterface.redraw();
			}
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			// TODO Auto-generated method stub
		}
	}
}
