package sineWavePanel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.border.LineBorder;

import audioPlayer.BcPlayer;

import javax.swing.border.EtchedBorder;

/**
 * 
 * @author Mathieu
 * Cette classe va seulement permettre de dessiner la repr�sentation du singal sinuso�dal, mais ne permettra pas d'autres interractions. 
 */

public class BcSineWaveBar extends JPanel {
	private float[] samplesArray;
	private float[] sampledDisplayed;
	private float maximumAmplitude;
	private float middleOfCanvas; //Le milieux du canevas
	private int nbPointsDisplayed; //On repr�sente un point par pixel en X;
	private int nbSamplesRepresentedOnScreen; //Le nombre de samples qui sont th�oriquement repr�sent�s � l'�cran
	private int maximumDisplayedSamples; //Le nombre maximum de samples pouvant �tre repr�sent�, quand on zoom au maximum
	private int centerOfZoom; //Le point � partir duquel la SineWave est zoom�e
	private int totalnbSamples;
	private int zoomValue;
	final private int nbPointsPerPixelsOfWidth = 2;
	
	//Attributs des �l�ments de la barre. 
	final private int buttonsWidth = 20;
	private JButton btnLeft;
	private JButton btnRight;
	private BcTimeBar panel;
	private BcScope scope;
	private BcSineWaveInterface sineInterface;
	final private BarListener listener = new BarListener();
	
	public BcSineWaveBar(int x, int y, int width, int height, BcPlayer player, float[] samplesArray, BcScope scope, BcSineWaveInterface sineInterface) {
		super();
		setBackground(new Color(214, 217, 223));
		super.setBounds(x, y, width, height);
		setLayout(null);
		setBorder(null);
		
		btnLeft = new JButton("");
		btnLeft.setBounds(0, 0, buttonsWidth, getHeight());
		add(btnLeft);
		
		int rightButtonPosition = getWidth() - buttonsWidth;
		btnRight = new JButton("");
		btnRight.setBounds(rightButtonPosition, 0,  buttonsWidth, getHeight());
		add(btnRight);
		
		int panelWidth = getWidth() - (2 * buttonsWidth);
		panel = new BcTimeBar(buttonsWidth, 0, panelWidth, getHeight(), player, samplesArray, scope, sineInterface);
		add(panel);
		
		this.scope = scope;
		btnLeft.addActionListener(listener);
		btnRight.addActionListener(listener);
		
		this.sineInterface = sineInterface;
	}
	
	public void updateTime(){
		panel.updateTime();
	}
	
	private class BarListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == btnLeft){
				scope.translateLeft();
				sineInterface.redraw();
			} else if(e.getSource() == btnRight){
				scope.translateRight();
				sineInterface.redraw();
			}
		}
	}
}