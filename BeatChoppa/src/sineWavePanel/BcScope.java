package sineWavePanel;

import java.awt.Color;
import java.awt.Graphics2D;

import audioPlayer.BcPlayer;

/**
 * 
 * @author Mathieu
 *	Cette m�thode sert � la classe BcZoomableSineWavePanel et BcTimerTab. Elle permet de garder une trace pour savoir
 *	o� est le d�but et la fin de la zone repr�sent�e. Elle permet �galement de calculer les zones repr�sent�s et de synchroniser
 *	la barre et le panel. C'est le pont entre les deux classes. 
 *
 *	Il deal seulement en samples. Les conversions sont faites dans les classes respectives.
 */

public class BcScope {
	private int startDisplayedPosition;
	private int endDisplayedPosition;
	private int cursorDisplayedPosition;
	private int startId;
	private int endId;
	private int cursorId;
	
	
	private double start;
	private double end;
	private double center;
	private double width;
	private double samplesLenght;
	private boolean keepWithCursor = false;
	private boolean drawCursorInMiddle = false;
	private BcPlayer player;
	final private double proportionOfSamplesTranslated = 1.0/6.0;
	final private double maximumLenghtToKeepWithCursor = 10.0;
	
	public BcScope(int samplesLenght, BcPlayer player) {
		this.start = 0;
		this.samplesLenght = samplesLenght;
		this.end = samplesLenght;
		this.player = player;
		width = end - start;
		center = width / 2;
	}
	
	public void calculateScopeFromStart(double start, int nbSamplesRepresentedOnScreen){
		this.start = start;
		this.width = (double) nbSamplesRepresentedOnScreen;
		this.center = this.start + (width/2);
		this.end = this.start + width;
	}
	
	public void calculateScopeFromEnd(double end, int nbSamplesRepresentedOnScreen){
		this.end = end;
		this.width = (double) nbSamplesRepresentedOnScreen;
		this.center = this.end - (width/2);
		this.start = this.end - width;
	}
	
	public void calculateNewScope(int nbSamplesRepresentedOnScreen, double focus){
		if(focus > 0){
			if(keepWithCursor && drawCursorInMiddle){
				// On veut zoomer � l'entour du centre.
				width = nbSamplesRepresentedOnScreen;
				start = center - width/2.0;
				end = center + width/2.0;
			} else {
				// On veut que le point de focus reste au m�me endroit sur l'�cran, mais que ce qui l'entoure zoom.
				double temporaryCenter =  width * focus + start;
				start = temporaryCenter - (double) nbSamplesRepresentedOnScreen * focus;
				end = start + nbSamplesRepresentedOnScreen;
				center = start + (nbSamplesRepresentedOnScreen/2);
				this.width = nbSamplesRepresentedOnScreen;
			}
			//Il faut v�rifier que l'on reste � l'int�rieur ce qui est visible.
			if(start < 0){
				calculateScopeFromStart(0, nbSamplesRepresentedOnScreen);
			}
			if(end > samplesLenght){
				calculateScopeFromEnd(samplesLenght, nbSamplesRepresentedOnScreen);
			}
		}
	}
	
	public boolean isKeepWithCursor() {
		return keepWithCursor;
	}

	public void setKeepWithCursor(boolean keepWithCursor) {
		this.keepWithCursor = keepWithCursor;
		if(!keepWithCursor){
			drawCursorInMiddle = false;
		}
	}

	public boolean isDrawCursorInMiddle() {
		return drawCursorInMiddle;
	}

	//�a sert essentiellement � redessiner l'image du sineWave quand on dessine le curseur au milieu. 
	//Il faut alors, rafraichir l'image � chaque appel. 
	public boolean updateTime(){
		//On ne veut pas rester avec le curseur si on d�zoome passer un certain point. Ce n'est pas commode.
		//Le mesure se fait en secondes.
		double lenghtRepresented = width / player.getAudioFormat().getFrameRate();
		if(this.keepWithCursor && lenghtRepresented < maximumLenghtToKeepWithCursor){
			double newCenter = player.getTimeManager().getCurrentTimeInSamples();
			double newStart = newCenter - (double)width/2.0;
			double newEnd = newStart + width;
			if(!(newStart < 0) && !(newEnd > samplesLenght)){
				center = newCenter;
				start = newStart;
				end = newEnd;
				drawCursorInMiddle = true;
				return true;
			}
		}
		drawCursorInMiddle = false;
		return false;
	}
	
	public void translateLeft(){
		if(!drawCursorInMiddle ){
			double translationValue = width * proportionOfSamplesTranslated;
			start -= translationValue;
			if(start < 0){
				start = 0;
				end = width;
				center = width/2;
			} else {
				end  -= translationValue;
				center -= translationValue;
			}
		}
	}
	
	public boolean isCursorVisible(){
		double sample = player.getTimeManager().getCurrentTimeInSamples();
		if(sample >start && sample < end){
			return true;
		} else {
			return false;
		}
	}
	
	public int getSongCursorPosition(){
		return (int) player.getTimeManager().getCurrentTimeInSamples();
	}
	
	public boolean isStartVisible(){
		double sample = player.getTimeManager().getStartInSamples();
		if(sample >start && sample < end){
			return true;
		} else {
			return false;
		}
	}
	
	public int getSongStartPosition(){
		return (int) player.getTimeManager().getStartInSamples();
	}
	
	public boolean isEndVisible(){
		double sample = player.getTimeManager().getEndInSamples();
		if(sample >start && sample < end){
			return true;
		} else {
			return false;
		}
	}
	
	public int getSongEndPosition(){
		return (int) player.getTimeManager().getEndInSamples();
	}
	
	public int getStartDisplayedPosition() {
		return startDisplayedPosition;
	}

	public void setStartDisplayedPosition(int startDisplayedPosition) {
		this.startDisplayedPosition = startDisplayedPosition;
	}

	public int getEndDisplayedPosition() {
		return endDisplayedPosition;
	}

	public void setEndDisplayedPosition(int endDisplayedPosition) {
		this.endDisplayedPosition = endDisplayedPosition;
	}

	public int getCursorDisplayedPosition() {
		return cursorDisplayedPosition;
	}

	public void setCursorDisplayedPosition(int cursorDisplayedPosition) {
		this.cursorDisplayedPosition = cursorDisplayedPosition;
	}

	public int getStartId() {
		return startId;
	}

	public void setStartId(int startId) {
		this.startId = startId;
	}

	public int getEndId() {
		return endId;
	}

	public void setEndId(int endId) {
		this.endId = endId;
	}

	public int getCursorId() {
		return cursorId;
	}

	public void setCursorId(int cursorId) {
		this.cursorId = cursorId;
	}

	public void translateRight(){
		if(!drawCursorInMiddle ){
			double translationValue = width * proportionOfSamplesTranslated;
			end += translationValue;
			if(end > samplesLenght){
				end = samplesLenght;
				start = end - width;
				center = start + width/2;
			} else {
				start += translationValue;
				center += translationValue;
			}
		}
	}
	
	public void translate(double relativeTranslation){
		if(!drawCursorInMiddle ){
			double translationValue = relativeTranslation * samplesLenght;
			if(translationValue < 0){
				double newStart = start + translationValue;
				if(!(newStart < 0.0)){
					start += translationValue;
					end += translationValue;
					center += translationValue;
				}
			} else {
				double newEnd = end + translationValue;
				if(!(newEnd > samplesLenght)){
					start += translationValue;
					end += translationValue;
					center += translationValue;
				}
			}
		}
	}


	public double getStart() {
		return start;
	}

	public double getEnd() {
		return end;
	}

	public double getCenter() {
		return center;
	}

	public double getWidth() {
		return width;
	}

	public double getSamplesLenght() {
		return samplesLenght;
	}
}
