BeatChoppa est un mini logiciel de station audionumérique (digital audio workstation (DAW)). Il permet essentiellement deux choses : la synthèse de fichiers audios et l’échantillonnage d’extraits audios. Il est possible d’appliquer plusieurs effets de synthèse sur un extrait audio pouvant être délimiter au sample prêt à partir d’un fichier audio de format WAVE mono canal. Une liste d’effets audio paramétrables ainsi qu’un panneau représentant l’onde sinusoïdale du fichier audio sont disponibles.
 
 L'application a été codée à l'aide du langage de programmation Java et à l'aide de la librairie TarsosDSP qui permet le traitement audio en temps
 
 Lien YouTube: https://www.youtube.com/watch?v=HABh1jc29Q4&feature=youtu.be


