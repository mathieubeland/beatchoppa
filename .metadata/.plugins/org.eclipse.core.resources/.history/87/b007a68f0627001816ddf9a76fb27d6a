package AudioPlayer;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.GainProcessor;
import be.tarsos.dsp.MultichannelToMono;
import be.tarsos.dsp.WaveformSimilarityBasedOverlapAdd;
import be.tarsos.dsp.WaveformSimilarityBasedOverlapAdd.Parameters;
import be.tarsos.dsp.effects.FlangerEffect;
import be.tarsos.dsp.filters.LowPassFS;
import be.tarsos.dsp.io.TarsosDSPAudioFormat;
import be.tarsos.dsp.io.jvm.AudioDispatcherFactory;
import be.tarsos.dsp.io.jvm.AudioPlayer;
import be.tarsos.dsp.io.jvm.WaveformWriter;
import be.tarsos.dsp.onsets.OnsetHandler;
import be.tarsos.dsp.onsets.PercussionOnsetDetector;
import be.tarsos.dsp.synthesis.AmplitudeLFO;
import be.tarsos.dsp.synthesis.SineGenerator;
import be.tarsos.dsp.writer.WaveHeader;
import be.tarsos.dsp.writer.WriterProcessor;

import AudioPlayer.BcAudioDispatcher;
import TarsosDSPModifiedClasses.ModifiedAudioDispatcherFactory;


public class BcPlayerChoppa {

	private PropertyChangeSupport support = new PropertyChangeSupport(this);
	private GainProcessor gainProcessor;
	private AudioPlayer audioPlayer;
	private BcAudioDispatcher dispatcher;
	private File loadedFile;
	private double gain; //Permet de contr�ler le volume
	private BcAudioTimeTracker time;
	private WaveformWriter writer;
	private BcLoopAudioProcessor loopProcessor;
	private AudioFileFormat fileFormat;
	private AudioFormat audioFormat;
	private BcPlayerState state;
	private double maximumGain;
	private static Timer timer;
	private Repeat task;

	public BcPlayerChoppa(File file, boolean loop){
		try{
			state = BcPlayerState.NO_FILE_LOADED;
			load(file);
			gain = 1.0;
			maximumGain = 2.0; //Une valeur de plus de 2 produit un bruit consid�rable.
			gainProcessor = new GainProcessor(gain);
			fileFormat = AudioSystem.getAudioFileFormat(file);
			audioFormat = fileFormat.getFormat();
		} catch (UnsupportedAudioFileException e) {
			throw new Error(e);
		} catch (IOException e) {
			throw new Error(e);
		}
	}
	
	public void load(File file) {
		if(state != BcPlayerState.NO_FILE_LOADED){
			eject();
		}
		loadedFile = file;
		AudioFileFormat fileFormat;
		try {
			fileFormat = AudioSystem.getAudioFileFormat(loadedFile);
		} catch (UnsupportedAudioFileException e) {
			throw new Error(e);
		} catch (IOException e) {
			throw new Error(e);
		}
		time = new BcAudioTimeTracker(fileFormat);
		setState(BcPlayerState.FILE_LOADED);
	}
	
	public void eject(){
		loadedFile = null;
		stop();
		setState(BcPlayerState.NO_FILE_LOADED);
	}
	
	public void stop(){
		if(state == BcPlayerState.PLAYING || state == BcPlayerState.PAUZED){
			setState(BcPlayerState.STOPPED);
			dispatcher.stop();
		} else if(state != BcPlayerState.STOPPED){
			throw new IllegalStateException("Can not stop when nothing is playing");
		}
	}
	
	public void play(double StartTime){
		time.setPausedAtInSeconds(StartTime);
		play();
	}
	
	public void play(long samples){
		time.setPausedAtInSamples(samples);
		play();
	}
	
	
	public void play(){
		if(state == BcPlayerState.NO_FILE_LOADED){
			throw new IllegalStateException("Can not play when no file is loaded");
		} else {
			initialise(false);
		}
	}

	public void initialise(boolean fromBeginning){
		if(state == BcPlayerState.NO_FILE_LOADED){
			throw new IllegalStateException("Can not play when no file is loaded");
		} else {
			try {
				//On peut ajouter autant de processors qu'on le veut pour faire des effets sur le son.
				dispatcher = ModifiedAudioDispatcherFactory.fromFile(loadedFile, 1024, 512);
				dispatcher.addAudioProcessor(gainProcessor);
				
				//Il faut toujours mettre l'AudioPlayer � la fin pour que les modifications s'appliquent lors de la lecture.
				//On doit �galement cr�er un nouvel audioPlayer � chaque initialisation.
				audioPlayer = new AudioPlayer(audioFormat);
				dispatcher.addAudioProcessor(audioPlayer);
				
				if(!fromBeginning) {
					dispatcher.skip(time.getPausedAtInSamples());
				}
				/*
				//On doit cr�er un nouveau Timer pour garder le compte du temps. 
				timer = new Timer();
				task = new Repeat();
				timer.schedule(task, 0, 100);
				*/
				Thread t = new Thread(dispatcher,"Audio Player Thread");
				t.start();
				setState(BcPlayerState.PLAYING);
			} catch (UnsupportedAudioFileException e) {
				throw new Error(e);
			} catch (IOException e) {
				throw new Error(e);
			}  catch (LineUnavailableException e) {
				throw new Error(e);
			}
		}
	}
	
	public void pause() {
		if(state == BcPlayerState.PLAYING || state == BcPlayerState.PAUZED){
			setState(BcPlayerState.PAUZED);
			time.setPausedAtInSamples(dispatcher.getSamplesProcessed());
			dispatcher.stop();
		} else {
			throw new IllegalStateException("Can not pause when nothing is playing");
		}
	}
	
	public void setGain(double newGain){
		gain = newGain;
		if(state == BcPlayerState.PLAYING ){
			gainProcessor.setGain(gain);
		}
	}
	
	public double getMaximumGain() {
		return maximumGain;
	}
	
	public BcAudioTimeTracker getTimeTracker(){
		if(state == BcPlayerState.NO_FILE_LOADED){
			throw new IllegalStateException("No file loaded, unable to determine the duration in seconds");
		}
		return time;
	}

	private void setState(BcPlayerState newState){
		BcPlayerState oldState = state;
		state = newState;
		support.firePropertyChange("state", oldState, newState);
	}
	
	public BcPlayerState getState() {
		return state;
	}

	public void addPropertyChangeListener(PropertyChangeListener l) {
		support.addPropertyChangeListener(l);
	}

	public void removePropertyChangeListener(PropertyChangeListener l) {
		support.removePropertyChangeListener(l);
	}
	
	private class Repeat extends TimerTask{
		@Override
		public void run() {
			time.setCurrentTimeInSamples(dispatcher.getSamplesProcessed());
		}
	}
}
