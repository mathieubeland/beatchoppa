package Syntheziser;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.Set;

import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import AudioEffects.BcAudioEffectsManager;
import AudioPlayer.BcPlayer;
import AudioPlayer.BcPlayerState;
import be.tarsos.dsp.AudioProcessor;
import util.Util;
import javax.swing.border.BevelBorder;

public class BcSynthesizer extends JFrame {

	private JPanel contentPane;
	private JTextField tfStart;
	private JTextField tfEnd;
	private JButton btnPlaypause;
	private JSlider slidergain;
	private JCheckBox chckbxLoop;
	private JLabel lblDuration;
	private JLabel lblStartTime;
	private JLabel lblEndTime;
	private JLabel lbltimetracker; 
	private JProgressBar progressBar;
	private JMenuItem menuItmSaveSample;
	private JScrollPane scrollPane;
	private JTable table;
	private JComboBox cBAvailableEffects;
	private DefaultComboBoxModel<String> availableEffectsCBModel;
	private BcAudioEffectsManager effectsManager;
	private JComboBox cBSynthesisEffects;
	private DefaultComboBoxModel<String> synthesisEffectsCBModel;
	private  Hashtable<String, AudioProcessor> processorsAvailable;
	private BcPlayer player;
	private Listener lst;
	private static final DecimalFormat df = new DecimalFormat( "#0.000" );
	private Timer timeThread;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BcSynthesizer frame = new BcSynthesizer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 * @throws UnsupportedAudioFileException 
	 */
	public BcSynthesizer() throws UnsupportedAudioFileException, IOException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1200, 307);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenuItem menuItmOpenFile = new JMenuItem("Open File");
		menuItmOpenFile.setBorder(new LineBorder(new Color(0, 0, 0)));
		menuItmOpenFile.setHorizontalAlignment(SwingConstants.CENTER);
		menuItmOpenFile.setHorizontalTextPosition(SwingConstants.CENTER);
		menuItmOpenFile.setMaximumSize(new Dimension(120, 32767));
		menuItmOpenFile.setSize(new Dimension(50, 0));
		menuBar.add(menuItmOpenFile);
		
		menuItmSaveSample = new JMenuItem("Save sample");
		menuItmSaveSample.setBorder(new LineBorder(new Color(0, 0, 0)));
		menuItmSaveSample.setHorizontalAlignment(SwingConstants.CENTER);
		menuItmSaveSample.setHorizontalTextPosition(SwingConstants.CENTER);
		menuItmSaveSample.setMaximumSize(new Dimension(120, 32767));
		menuBar.add(menuItmSaveSample);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAvailableEffects = new JLabel("Add synthesis effect:");
		lblAvailableEffects.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAvailableEffects.setBounds(991, 2, 183, 37);
		contentPane.add(lblAvailableEffects);
		
		availableEffectsCBModel = new DefaultComboBoxModel<String>();
		cBAvailableEffects = new JComboBox(availableEffectsCBModel);
		cBAvailableEffects.setBounds(833, 42, 341, 20);
		contentPane.add(cBAvailableEffects);
		
		JLabel lblBeginning = new JLabel("Start: ");
		lblBeginning.setBounds(637, 11, 45, 37);
		contentPane.add(lblBeginning);
		
		JLabel lblEnd = new JLabel("End:");
		lblEnd.setBounds(637, 42, 45, 37);
		contentPane.add(lblEnd);
		
		tfStart = new JTextField();
		tfStart.setBounds(678, 19, 66, 20);
		contentPane.add(tfStart);
		tfStart.setColumns(10);
		
		tfEnd = new JTextField();
		tfEnd.setBounds(678, 50, 66, 20);
		contentPane.add(tfEnd);
		tfEnd.setColumns(10);
		
		chckbxLoop = new JCheckBox("loop");
		chckbxLoop.setBounds(55, 18, 52, 23);
		contentPane.add(chckbxLoop);
		
		slidergain = new JSlider();
		slidergain.setMaximum(200);
		slidergain.setValue(100);
		slidergain.setBounds(10, 83, 149, 26);
		contentPane.add(slidergain);
		
		JLabel lblGain = new JLabel("Gain:");
		lblGain.setBounds(10, 54, 73, 25);
		contentPane.add(lblGain);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 135, 1164, 100);
		contentPane.add(scrollPane);
		
		lblStartTime = new JLabel("--");
		lblStartTime.setText("00:00:000");
		lblStartTime.setHorizontalTextPosition(SwingConstants.CENTER);
		lblStartTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblStartTime.setBounds(753, 11, 70, 36);
		contentPane.add(lblStartTime);
		
		lblEndTime = new JLabel("--");
		lblEndTime.setText("00:00:000");
		lblEndTime.setHorizontalTextPosition(SwingConstants.CENTER);
		lblEndTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblEndTime.setBounds(754, 42, 69, 36);
		contentPane.add(lblEndTime);
		
		JPanel playerPanel = new JPanel();
		playerPanel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		playerPanel.setBounds(165, 11, 462, 113);
		contentPane.add(playerPanel);
		playerPanel.setLayout(null);
		
		btnPlaypause = new JButton("play/pause");
		btnPlaypause.setBounds(20, 29, 85, 23);
		playerPanel.add(btnPlaypause);
		
		progressBar = new JProgressBar();
		progressBar.setValue(0);
		progressBar.setBounds(10, 7, 442, 11);
		playerPanel.add(progressBar);
		
		lbltimetracker = new JLabel("- - : - -");
		lbltimetracker.setHorizontalAlignment(SwingConstants.RIGHT);
		lbltimetracker.setBounds(345, 31, 49, 19);
		playerPanel.add(lbltimetracker);
		
		JLabel lblBackSlash = new JLabel(" / ");
		lblBackSlash.setBounds(399, 31, 10, 19);
		playerPanel.add(lblBackSlash);
		
		lblDuration = new JLabel("- - : - -");
		lblDuration.setBounds(414, 29, 38, 23);
		playerPanel.add(lblDuration);
		lblDuration.setHorizontalTextPosition(SwingConstants.LEFT);
		lblDuration.setHorizontalAlignment(SwingConstants.LEFT);
		
		JLabel lblSynthesisEffects = new JLabel("Synthesis effect:");
		lblSynthesisEffects.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSynthesisEffects.setBounds(991, 61, 183, 37);
		contentPane.add(lblSynthesisEffects);
		
		synthesisEffectsCBModel = new DefaultComboBoxModel<String>();
		cBSynthesisEffects = new JComboBox(synthesisEffectsCBModel);
		cBSynthesisEffects.setBounds(833, 96, 341, 20);
		contentPane.add(cBSynthesisEffects);
		
		
		//Nouveau lecteur 
		File file = new File("Murder_was_the_case_mono.wav");		
		//File file = new File("Jelek.wav");	
		//File file = new File("NoVaseline.wav");	
		//File file = new File("FallingAwayFromMe.wav");
		
		player = new BcPlayer(file);
		player.setLooping(chckbxLoop.isSelected());
		effectsManager = new BcAudioEffectsManager(player);
		
		//Indiquer les effets qui sont disponibles
		for(String effects:effectsManager.getAvailableProcessors()) {
			availableEffectsCBModel.addElement(effects);
		}
		
		//On connecte l'�couteur aux widgets
		lst = new Listener();
		slidergain.addChangeListener(lst);
		tfStart.addActionListener(lst);
		tfEnd.addActionListener(lst);
		btnPlaypause.addActionListener(lst);
		progressBar.addMouseListener(lst);
		chckbxLoop.addActionListener(lst);
		menuItmSaveSample.addActionListener(lst);
		cBAvailableEffects.addActionListener(lst);
		cBSynthesisEffects.addActionListener(lst);
		initialiseProgressBar();
		
		//On cr�e le thread qui va mettre la barre de progression et le temps � jour
		timeThread = new Timer(1000, lst);
		timeThread.start();
		
		//La liste qui contient les processeurs cr��s
		processorsAvailable = new Hashtable<String, AudioProcessor>();
		
		//Choses que je vais devoir initialiser une fois que je vais choisir le fichier moi-m�me
		lblDuration.setText(String.valueOf((Util.secondsToString(player.getTimeManager().getDurationInSeconds()))));
		tfStart.setText(String.valueOf((df.format(player.getTimeManager().getStartAtInSeconds()))));
		tfEnd.setText(String.valueOf(df.format(player.getTimeManager().getEndsAtInSeconds())));
		lblStartTime.setText(Util.milisecondsToString(player.getTimeManager().getStartAtInSeconds()));
		lblEndTime.setText(Util.milisecondsToString(player.getTimeManager().getEndsAtInSeconds()));
		lbltimetracker.setText(Util.secondsToString(player.getTimeManager().getCurrentTimeInSeconds()));
	}
	
	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public BcAudioEffectsManager getEffectsManager() {
		return effectsManager;
	}
	
	private void initialiseProgressBar(){
		progressBar.setMaximum((int) player.getTimeManager().getDurationInSeconds());
	}
	
	private void updateTime(){
		lbltimetracker.setText(Util.secondsToString(player.getTimeManager().getCurrentTimeInSeconds()));
		progressBar.setValue((int) player.getTimeManager().getCurrentTimeInSeconds());
	}
	
	private void updateTime(int positionOnProgressBar){
		double newPosition = (double)positionOnProgressBar / (double)progressBar.getWidth() * player.getTimeManager().getDurationInSeconds();
		player.play(newPosition);
		updateTime();
	}
	
	public void updateSynthesisEffectsList(){
		cBSynthesisEffects.removeActionListener(lst);
		synthesisEffectsCBModel.removeAllElements();
		processorsAvailable = effectsManager.getInstantiatedProcessors();
		Set<String> keys = processorsAvailable.keySet();
		for(String effect:keys) {
			System.out.println("key: " + effect);
			synthesisEffectsCBModel.addElement(effect);
		}
		cBSynthesisEffects.addActionListener(lst);
	}
	
	private void createEffectPanel(String selectedEffect){
		scrollPane.setViewportView(effectsManager.createEffectPanel(processorsAvailable.get(selectedEffect), BcSynthesizer.this));
	}
	
	private class Listener implements ActionListener, ChangeListener, MouseListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == btnPlaypause){
				if(player.getState() == BcPlayerState.PLAYING){
					player.pause();					
				} else if (player.getState() == BcPlayerState.PAUZED || player.getState() == BcPlayerState.FILE_LOADED || player.getState() == BcPlayerState.ENDED){
					player.play();					
				} 
			} else if(e.getSource() == tfStart){
				String start = tfStart.getText();
				if(Util.sanitizeStartOrEnd(BcSynthesizer.this, "start", start, player.getTimeManager().getStartAtInSeconds(), player.getTimeManager().getEndsAtInSeconds(), player.getTimeManager().getDurationInSeconds())){
					player.getTimeManager().setStartAtInSeconds(Double.parseDouble(start));
					lblStartTime.setText(Util.milisecondsToString(player.getTimeManager().getStartAtInSeconds()));
				} else {
					tfStart.setText(String.valueOf((df.format(player.getTimeManager().getStartAtInSeconds()))));
				}
			} else if(e.getSource() == tfEnd){
				String end = tfEnd.getText();
				if(Util.sanitizeStartOrEnd(BcSynthesizer.this, "end", end, player.getTimeManager().getStartAtInSeconds(), player.getTimeManager().getEndsAtInSeconds(), player.getTimeManager().getDurationInSeconds())){
					player.getTimeManager().setEndsAtInSeconds(Double.parseDouble(end));
					lblEndTime.setText(Util.milisecondsToString(player.getTimeManager().getEndsAtInSeconds()));
				} else {
					tfEnd.setText(String.valueOf(df.format(player.getTimeManager().getEndsAtInSeconds())));
				}
			} else if(e.getSource() == timeThread){
				updateTime();
			} else if(e.getSource() == chckbxLoop){
				player.setLooping(chckbxLoop.isSelected());
			} else if(e.getSource() == menuItmSaveSample){
				player.record();
			} else if(e.getSource() == cBAvailableEffects){
				String processor = cBAvailableEffects.getSelectedItem().toString();
				System.out.println(processor);
				effectsManager.addAudioProcessor(processor);
				updateSynthesisEffectsList();
				String selectedEffect = cBSynthesisEffects.getSelectedItem().toString();
				createEffectPanel(selectedEffect);
			} else if(e.getSource() == cBSynthesisEffects){
				String selectedEffect = cBSynthesisEffects.getSelectedItem().toString();
				createEffectPanel(selectedEffect);
			}
		}

		@Override
		public void stateChanged(ChangeEvent e) {
			if(e.getSource() == slidergain){
				//Il faut retourner une valeur entre 0 et 5;
				double interval = slidergain.getMaximum() - slidergain.getMinimum();
				double middle = interval / 2;
				double slider = slidergain.getValue();
				double newGain = 0;
				if(slider == middle){
					newGain = 1.0;
					player.setGain(newGain);
				} else if (slider > middle){
					newGain = 1.0 + ((player.getMaximumGain() - 1.0) * (slider - middle)/middle);
					player.setGain(newGain);
				} else if (slider < middle){
					newGain = slider/middle;
					player.setGain(newGain);
				}
			}
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			if(e.getSource() == progressBar){
				updateTime(e.getX());
			}
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	}
}


