package AudioPlayer;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import AudioEffects.BcAudioEffectInfos;
import AudioEffects.BcFlangerEffectPanel;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.FadeIn;
import be.tarsos.dsp.GainProcessor;
import be.tarsos.dsp.MultichannelToMono;
import be.tarsos.dsp.WaveformSimilarityBasedOverlapAdd;
import be.tarsos.dsp.WaveformSimilarityBasedOverlapAdd.Parameters;
import be.tarsos.dsp.effects.FlangerEffect;
import be.tarsos.dsp.filters.LowPassFS;
import be.tarsos.dsp.granulator.Granulator;
import be.tarsos.dsp.granulator.OptimizedGranulator;
import be.tarsos.dsp.io.TarsosDSPAudioFormat;
import be.tarsos.dsp.io.jvm.AudioDispatcherFactory;
import be.tarsos.dsp.io.jvm.AudioPlayer;
import be.tarsos.dsp.io.jvm.WaveformWriter;
import be.tarsos.dsp.onsets.OnsetHandler;
import be.tarsos.dsp.onsets.PercussionOnsetDetector;
import be.tarsos.dsp.synthesis.AmplitudeLFO;
import be.tarsos.dsp.synthesis.SineGenerator;
import be.tarsos.dsp.writer.WaveHeader;
import be.tarsos.dsp.writer.WriterProcessor;

import AudioPlayer.BcAudioDispatcher;
import TarsosDSPModifiedClasses.ModifiedAudioDispatcher;
import TarsosDSPModifiedClasses.ModifiedAudioDispatcherFactory;
import TarsosDSPModifiedClasses.ModifiedWaveformSimilarityBasedOverlapAdd;


public class BcPlayer{
	
	private PropertyChangeSupport support = new PropertyChangeSupport(this);
	private GainProcessor gainProcessor;
	private AudioPlayer audioPlayer;
	private BcAudioDispatcher dispatcher;
	private File loadedFile;
	private double gain; //Permet de contr�ler le volume
	private WaveformWriter writer;
	private AudioFileFormat fileFormat;
	private AudioFormat audioFormat;
	private BcPlayerState state;
	private double maximumGain;
	private Thread mainThread;
	private BcTimeManager time;
	private double sampleRate;
	private boolean timeStretch;
	private double tempo;
	private ModifiedWaveformSimilarityBasedOverlapAdd wsola;
	final private int bufferSize = 1024;
	protected List<AudioProcessor> audioProcessors;

	public BcPlayer(File file){
			state = BcPlayerState.NO_FILE_LOADED;
			load(file);
			gain = 1.0;
			tempo = 1.0;
			timeStretch = false;
			
			audioProcessors = new CopyOnWriteArrayList<AudioProcessor>();
			maximumGain = 2.0; //Une valeur de plus de 2 produit un bruit consid�rable.
			gainProcessor = new GainProcessor(gain);
	}
	
	public ModifiedWaveformSimilarityBasedOverlapAdd getWsola() {
		return wsola;
	}

	public void setWsola(ModifiedWaveformSimilarityBasedOverlapAdd wsola) {
		this.wsola = wsola;
	}

	public boolean isTimeStretch() {
		return timeStretch;
	}

	public void setTimeStretch(boolean timeStretch) {
		this.timeStretch = timeStretch;
	}

	public double getSampleRate() {
		return sampleRate;
	}

	public void setSampleRate(double sampleRate) {
		this.sampleRate = sampleRate;
	}
	
	public AudioProcessor getProcessorByIndex(int index){
		return audioProcessors.get(index);
	}
	
	public int getNumberOfProcessors(){
		return audioProcessors.size();
	}
	
	public void addAudioProcessor(AudioProcessor audioProcessor) {
		if(state == BcPlayerState.PLAYING){ 
			pause();
			this.audioProcessors.add(audioProcessor);
			play();
		} else {
			this.audioProcessors.add(audioProcessor);
		}
	}
	
	public double getTempo() {
		return tempo;
	}

	public void setTempo(double tempo) {
		if(state == BcPlayerState.PLAYING){ 
			pause();
			this.tempo = tempo;
			play();
		} else {
			this.tempo = tempo;
		}
	}

	public void deleteAudioProcessor(AudioProcessor audioProcessors) {
		this.audioProcessors.remove(audioProcessors);
	}
	
	public List<AudioProcessor> getAudioProcessors() {
		return this.audioProcessors;
	}

	public BcAudioDispatcher getDispatcher() {
		return dispatcher;
	}

	public void load(File file) {
		if(state != BcPlayerState.NO_FILE_LOADED){
			eject();
		}
		loadedFile = file;
		try {
			fileFormat = AudioSystem.getAudioFileFormat(loadedFile);
			audioFormat = fileFormat.getFormat();
			sampleRate = AudioSystem.getAudioInputStream(loadedFile).getFormat().getSampleRate();
		} catch (UnsupportedAudioFileException e) {
			throw new Error(e);
		} catch (IOException e) {
			throw new Error(e);
		}
		setState(BcPlayerState.FILE_LOADED);
		//Certaine chose ne peuvent �tre initialis�e qu'en fonction du fichier s�lectionn�. 
		time = new BcTimeManager(fileFormat, BcPlayer.this, false);
	}
	
	public void eject(){
		loadedFile = null;
		stop();
		setState(BcPlayerState.NO_FILE_LOADED);
	}
	
	public void stop(){
		if(state == BcPlayerState.PLAYING || state == BcPlayerState.PAUZED){
			setState(BcPlayerState.STOPPED);
			dispatcher.stop();
		} else if(state != BcPlayerState.STOPPED){
			throw new IllegalStateException("Can not stop when nothing is playing");
		}
	}
	
	public void play(double StartTime){
		time.setResumeAtInSeconds(StartTime);
		play();
	}
	
	public void play(long samples){
		time.setResumeAtInSamples(samples);
		play();
	}
	
	public void play(){
		if(state == BcPlayerState.NO_FILE_LOADED){
			throw new IllegalStateException("Can not play when no file is loaded");
		} else {
			if(dispatcher == null || dispatcher.isEnded()){
				initialise(true, false);
			} else {
				initialise(false, false);
			}
		}
	}

	public void record(){
		if(state == BcPlayerState.NO_FILE_LOADED){
			throw new IllegalStateException("Can not play when no file is loaded");
		} else {
			initialise(true, true);
		}
	}

	public void initialise(boolean fromBeginning, boolean createWAV){
		if(state == BcPlayerState.NO_FILE_LOADED){
			throw new IllegalStateException("Can not play when no file is loaded");
		} else {
			try {
				//On peut ajouter autant de processors qu'on le veut pour faire des effets sur le son.
				if(dispatcher != null){
					dispatcher.stop(); //Il faut arr�ter le dispatcher si on veut que la lecture se termine et que le garbage collector
									   //supprime �ventuellement le dispatcher qui ro�le sur le thread. 
				}
				
				//Le WaveformSimilarityBasedOverlapAdd permet de faire tu time stretching et est diff�rent des autres processeurs. 
				//On ne peut pas en mettre deux et on doit lui passer le dispatcher parce qu'il modifie le sampleRate. 
				if(timeStretch){
					wsola = new ModifiedWaveformSimilarityBasedOverlapAdd(Parameters.slowdownDefaults(tempo,audioFormat.getSampleRate()));
					dispatcher = ModifiedAudioDispatcherFactory.fromFile(loadedFile, wsola.getInputBufferSize(), wsola.getOverlap());
					wsola.setDispatcher(dispatcher);
					dispatcher.addAudioProcessor(wsola);
				} else {
					dispatcher = ModifiedAudioDispatcherFactory.fromFile(loadedFile, bufferSize, bufferSize/2);
				}
				
				dispatcher.setPlayer(BcPlayer.this);
				dispatcher.addAudioProcessor(gainProcessor);
				dispatcher.addAudioProcessor(time);
				
				//On ajoute les processeurs que l'utilisateur peut ajouter lui-m�me
				for(AudioProcessor effect:this.audioProcessors){
					dispatcher.addAudioProcessor(effect);
				}
				
				//Il faut toujours mettre l'AudioPlayer ou le WaveFormWriter � la fin pour que les modifications s'appliquent lors de la lecture ou de l'�criture du nouveau fichier.
				if(createWAV){
					//On doit lire du d�but � la fin sans loop. On cr�e donc un nouveau loop processor qui ne va pas influencer le cours du loopProcessor principal.
					writer = new WaveformWriter(audioFormat,"yo.wav");
					dispatcher.addAudioProcessor(writer);
					time.setLooping(false);
				}else{
					//On doit �galement cr�er un nouvel audioPlayer � chaque initialisation.
					//Il est TR�S important de passer en param�tre le longeur du buffer, car c'est ce qui permet de bien synchroniser des �v�nements et le son.
					if(timeStretch){
						audioPlayer = new AudioPlayer(audioFormat, wsola.getInputBufferSize());
					} else {
						audioPlayer = new AudioPlayer(audioFormat, bufferSize);
					}
					dispatcher.addAudioProcessor(audioPlayer);
				}
				if(fromBeginning) {
					dispatcher.skip(time.getStartAtInSeconds());
				} else {
					dispatcher.skip(time.getResumeAtInSamples());
				}
				//On doit passer le nouveau dispatcher au TimeTracker pour qu'il puisse g�rer le temps. 
				mainThread = new Thread(dispatcher,"Audio Player Thread");
				mainThread.start();
				setState(BcPlayerState.PLAYING);
			} catch (UnsupportedAudioFileException e) {
				throw new Error(e);
			} catch (IOException e) {
				throw new Error(e);
			}  catch (LineUnavailableException e) {
				throw new Error(e);
			}
			state = BcPlayerState.PLAYING;
		}
	}
	
	public void pause() {
		if(state == BcPlayerState.PLAYING){ //|| state == BcPlayerState.PAUZED){ //Je ne pense pas que �a servait � grand chose...
			setState(BcPlayerState.PAUZED);
			time.setResumeAtInSamples(dispatcher.getSamplesProcessed());
			dispatcher.stop();
		} else {
			throw new IllegalStateException("Can not pause when nothing is playing");
		}
	}
	
	public void setGain(double newGain){
		gain = newGain;
		if(state == BcPlayerState.PLAYING ){
			gainProcessor.setGain(gain);
		}
	}
	
	public double getMaximumGain() {
		return maximumGain;
	}
	
	public BcTimeManager getTimeManager(){
		if(state == BcPlayerState.NO_FILE_LOADED){
			throw new IllegalStateException("No file loaded, unable to determine the duration in seconds");
		}
		return time;
	}
	
	public boolean isLooping(){
		return this.loop;
	}
	
	public void setLooping(boolean loop){
		time.setLooping(loop);
		this.loop = loop;
	}

	public void setState(BcPlayerState newState){
		BcPlayerState oldState = state;
		state = newState;
		support.firePropertyChange("state", oldState, newState);
	}
	
	public BcPlayerState getState() {
		return state;
	}

	public void addPropertyChangeListener(PropertyChangeListener l) {
		support.addPropertyChangeListener(l);
	}

	public void removePropertyChangeListener(PropertyChangeListener l) {
		support.removePropertyChangeListener(l);
	}
}
