import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import AudioPlayer.BcPlayerChoppa;
import AudioPlayer.BcPlayerState;
import util.Util;
import javax.swing.border.BevelBorder;

public class BcSynthesizer extends JFrame {

	private JPanel contentPane;
	private JTextField tfStart;
	private JTextField tfEnd;
	private JButton btnPlaypause;
	private JSlider slidergain;
	private JCheckBox chckbxLoop;
	private JLabel lblDuration;
	private JLabel lblStartTime;
	private JLabel lblEndTime;
	private JLabel lbltimetracker; 
	private JSlider sliderProgress;
	
	private BcPlayerChoppa player;
	private Listener lst;
	public static final DecimalFormat df = new DecimalFormat( "#0.000" );
	private timeElapsedThread timeThread;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BcSynthesizer frame = new BcSynthesizer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 * @throws UnsupportedAudioFileException 
	 */
	public BcSynthesizer() throws UnsupportedAudioFileException, IOException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1200, 700);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenuItem menuItmOpenFile = new JMenuItem("Open File");
		menuItmOpenFile.setBorder(new LineBorder(new Color(0, 0, 0)));
		menuItmOpenFile.setHorizontalAlignment(SwingConstants.CENTER);
		menuItmOpenFile.setHorizontalTextPosition(SwingConstants.CENTER);
		menuItmOpenFile.setMaximumSize(new Dimension(120, 32767));
		menuItmOpenFile.setSize(new Dimension(50, 0));
		menuBar.add(menuItmOpenFile);
		
		JMenuItem menuItmSaveSample = new JMenuItem("Save sample");
		menuItmSaveSample.setBorder(new LineBorder(new Color(0, 0, 0)));
		menuItmSaveSample.setHorizontalAlignment(SwingConstants.CENTER);
		menuItmSaveSample.setHorizontalTextPosition(SwingConstants.CENTER);
		menuItmSaveSample.setMaximumSize(new Dimension(120, 32767));
		menuBar.add(menuItmSaveSample);
		
		JMenuItem menuItmSampler = new JMenuItem("Sampler");
		menuItmSampler.setBorder(new LineBorder(new Color(0, 0, 0)));
		menuItmSampler.setHorizontalAlignment(SwingConstants.CENTER);
		menuItmSampler.setHorizontalTextPosition(SwingConstants.CENTER);
		menuItmSampler.setMaximumSize(new Dimension(120, 32767));
		menuBar.add(menuItmSampler);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(34, 135, 1164, 489);
		contentPane.add(scrollPane);
		
		JLabel lblSynthesis = new JLabel("Add synthesis effect:");
		lblSynthesis.setBounds(1060, 11, 114, 37);
		contentPane.add(lblSynthesis);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(816, 50, 358, 20);
		contentPane.add(comboBox);
		
		JLabel lblBeginning = new JLabel("Start: ");
		lblBeginning.setBounds(637, 11, 45, 37);
		contentPane.add(lblBeginning);
		
		JLabel lblEnd = new JLabel("End:");
		lblEnd.setBounds(637, 42, 45, 37);
		contentPane.add(lblEnd);
		
		tfStart = new JTextField();
		tfStart.setBounds(678, 19, 66, 20);
		contentPane.add(tfStart);
		tfStart.setColumns(10);
		
		tfEnd = new JTextField();
		tfEnd.setBounds(678, 50, 66, 20);
		contentPane.add(tfEnd);
		tfEnd.setColumns(10);
		
		chckbxLoop = new JCheckBox("loop");
		chckbxLoop.setBounds(55, 18, 52, 23);
		contentPane.add(chckbxLoop);
		
		slidergain = new JSlider();
		slidergain.setMaximum(200);
		slidergain.setValue(100);
		slidergain.setBounds(10, 83, 149, 26);
		contentPane.add(slidergain);
		
		JLabel lblGain = new JLabel("Gain:");
		lblGain.setBounds(10, 54, 73, 25);
		contentPane.add(lblGain);
		
		lblStartTime = new JLabel("--");
		lblStartTime.setHorizontalTextPosition(SwingConstants.CENTER);
		lblStartTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblStartTime.setBounds(753, 11, 52, 36);
		contentPane.add(lblStartTime);
		
		lblEndTime = new JLabel("--");
		lblEndTime.setHorizontalTextPosition(SwingConstants.CENTER);
		lblEndTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblEndTime.setBounds(754, 42, 52, 36);
		contentPane.add(lblEndTime);
		
		JPanel playerPanel = new JPanel();
		playerPanel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		playerPanel.setBounds(165, 11, 462, 113);
		contentPane.add(playerPanel);
		playerPanel.setLayout(null);
		
		btnPlaypause = new JButton("play/pause");
		btnPlaypause.setBounds(20, 44, 85, 23);
		playerPanel.add(btnPlaypause);
		
		sliderProgress = new JSlider();
		sliderProgress.setValue(0);
		sliderProgress.setBounds(10, 7, 442, 26);
		playerPanel.add(sliderProgress);
		
		lbltimetracker = new JLabel("0:0:0");
		lbltimetracker.setHorizontalAlignment(SwingConstants.RIGHT);
		lbltimetracker.setBounds(336, 46, 49, 19);
		playerPanel.add(lbltimetracker);
		
		JLabel lblBackSlash = new JLabel(" / ");
		lblBackSlash.setBounds(387, 46, 10, 19);
		playerPanel.add(lblBackSlash);
		
		lblDuration = new JLabel("- - : - - : --");
		lblDuration.setBounds(403, 37, 49, 36);
		playerPanel.add(lblDuration);
		lblDuration.setHorizontalTextPosition(SwingConstants.LEFT);
		lblDuration.setHorizontalAlignment(SwingConstants.LEFT);
		
		
		//On connecte l'�couteur aux widgets
		lst = new Listener();
		slidergain.addChangeListener(lst);
		tfStart.addActionListener(lst);
		tfEnd.addActionListener(lst);
		btnPlaypause.addActionListener(lst);
		
		//Nouveau lecteur 
		File file = new File("Murder_was_the_case_mono.wav");		
		player = new BcPlayerChoppa(file, chckbxLoop.isSelected());
		
		//On cr�e le thread qui va mettre la barre de progression et le temps � jour
		timeThread

		
		//Choses que je vais devoir initialiser une fois que je vais choisir le fichier moi-m�me
		lblDuration.setText(String.valueOf((Util.secondsToString(player.getTimeTracker().getDurationInSeconds()))));
		tfStart.setText(String.valueOf((df.format(player.getTimeTracker().getStartAtInSeconds()))));
		tfEnd.setText(String.valueOf(df.format(player.getTimeTracker().getEndsAtInSeconds())));
		lblStartTime.setText(Util.secondsToString(player.getTimeTracker().getStartAtInSeconds()));
		lblEndTime.setText(Util.secondsToString(player.getTimeTracker().getEndsAtInSeconds()));
	}
	
	private class Listener implements ActionListener, ChangeListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == btnPlaypause){
				if(player.getState() == BcPlayerState.PLAYING){
					player.pause();					
				} else if (player.getState() == BcPlayerState.PAUZED || player.getState() == BcPlayerState.FILE_LOADED){
					player.play();					
				} 
			} else if(e.getSource() == tfStart){
				String start = tfStart.getText();
				System.out.println(start);
				if(Util.sanitizeStartOrEnd(BcSynthesizer.this, start, player.getTimeTracker().getEndsAtInSeconds(), true)){
					player.getTimeTracker().setStartAtInSeconds(Double.parseDouble(start));
				} else {
					tfStart.setText(String.valueOf((df.format(player.getTimeTracker().getStartAtInSeconds()))));
				}
			} else if(e.getSource() == tfEnd){
				String end = tfEnd.getText();
				System.out.println(end);
				if(Util.sanitizeStartOrEnd(BcSynthesizer.this, end, player.getTimeTracker().getStartAtInSeconds(), false)){
					player.getTimeTracker().setEndsAtInSeconds(Double.parseDouble(end));
				} else {
					tfEnd.setText(String.valueOf(df.format(player.getTimeTracker().getEndsAtInSeconds())));
				}
			}
		}

		@Override
		public void stateChanged(ChangeEvent e) {
			if(e.getSource() == slidergain){
				//Il faut retourner une valeur entre 0 et 5;
				double interval = slidergain.getMaximum() - slidergain.getMinimum();
				double middle = interval / 2;
				double slider = slidergain.getValue();
				double newGain = 0;
				if(slider == middle){
					newGain = 1.0;
					player.setGain(newGain);
				} else if (slider > middle){
					newGain = 1.0 + ((player.getMaximumGain() - 1.0) * (slider - middle)/middle);
					player.setGain(newGain);
				} else if (slider < middle){
					newGain = slider/middle;
					player.setGain(newGain);
				}
			}
		}
	}
	
    public class timeElapsedThread extends Thread{
    	private JLabel lbltimetracker;
    	private JSlider sliderProgress;
    	private BcPlayerChoppa player;
    	
		public void run() {
			lbltimetracker.setText(Util.secondsToString(player.getTimeTracker().getCurrentTimeInSeconds()));
		}
		 
		public void main(BcPlayerChoppa player, JLabel lbltimetracker, JSlider sliderProgress) {
			this.lbltimetracker = lbltimetracker;
			this.sliderProgress = sliderProgress;
			this.player = player;
		}
    }
}


