package AudioPlayer;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;

import TarsosDSPModifiedClasses.ModifiedAudioDispatcher;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.io.TarsosDSPAudioInputStream;

/**
 * La classe h�rite d'AudioDispatcher qui a �t� l�g�rement modifi�e pour que l'on puisse utiliser 
 * les attriuts de la classes (private chang� pour protected). On a �galement modifi� la fonction 
 * run pour pouvoir indiquer que la chanson s'est termin�e (Ended). 
 * 
 * Nous avons �galement fait le sacril�ge de modifier largement cette Classe pour pouvoir avoir
 * beaucoup de pr�cision pour pouvoir faire looper le fichier audio jou�. 
 */

public class BcAudioDispatcher extends ModifiedAudioDispatcher  {
	//Le Dispatcher va looper seulement quand il va avoir un AudioPlayer comme processeur, car sinon on enregistre,<
	//donc on ne veut pas que le processeur loop �ternellement. 
	private boolean audioPlayer = false;
	private  BcPlayer player;
	private boolean ended = false;
	
	public BcAudioDispatcher(TarsosDSPAudioInputStream stream, int audioBufferSize, int bufferOverlap, BcPlayer player) {
		super(stream, audioBufferSize, bufferOverlap);
	}
	
	// On doit absolument set le player. On ne l'as pas exiger dans le constructeur, car
	// c'est le 
	public void setPlayer(BcPlayer player){
		this.player = player;
	}
	
	public boolean isEnded() {
		return ended;
	}

	public void setEnded(boolean ended) {
		this.ended = ended;
	}
	
	public void skip(long frames){
		bytesToSkip = frames * format.getFrameSize(); 
	}

	public long getSamplesProcessed(){
		return Math.round(bytesProcessed/format.getFrameSize());
	}
	
	public List<AudioProcessor> getProcessorsList(){
		return audioProcessors;
	}
	
	public void stop() {
		stopped = true;
		for (final AudioProcessor processor : audioProcessors) {
			processor.processingFinished();
		}
		try {
			audioInputStream.close();
		} catch (IOException e) {
			LOG.log(Level.SEVERE, "Closing audio stream error.", e);
		}
	}
	
	public void deleteAudioProcessor(final AudioProcessor audioProcessor) {
		audioProcessors.remove(audioProcessor);
	}
	
	@Override
	public void run() {
		
		int bytesRead = 0;
		
		if(bytesToSkip!=0){
			skipToStart();
		}
	
		//Read the first (and in some cases last) audio block.
		try {
			//needed to get correct time info when skipping first x seconds
			audioEvent.setBytesProcessed(bytesProcessed);
			bytesRead = readNextAudioBlock();
		} catch (IOException e) {
			String message="Error while reading audio input stream: " + e.getMessage();	
			LOG.warning(message);
			throw new Error(message);
		}

		// As long as the stream has not ended
		while (bytesRead != 0 && !stopped) {
			
			//Makes sure the right buffers are processed, they can be changed by audio processors.
			for (final AudioProcessor processor : audioProcessors) {
				//System.out.println(processor);
				if(!processor.process(audioEvent)){
					//skip to the next audio processors if false is returned.
					break;
				}	
			}
			
			if(!stopped){			
				//Update the number of bytes processed;
				bytesProcessed += bytesRead;
				audioEvent.setBytesProcessed(bytesProcessed);
					
				// Read, convert and process consecutive overlapping buffers.
				// Slide the buffer.
				try {
					bytesRead = readNextAudioBlock();
					audioEvent.setOverlap(floatOverlap);
				} catch (IOException e) {
					String message="Error while reading audio input stream: " + e.getMessage();	
					LOG.warning(message);
					throw new Error(message);
				}
			}
		}

		// Notify all processors that no more data is available. 
		// when stop() is called processingFinished is called explicitly, no need to do this again.
		// The explicit call is to prevent timing issues.
		if(!stopped){
			this.ended = true;
			/*C'est essentiellement ce qui a �t� ajout�*/
			player.setState(BcPlayerState.ENDED);
			stop();
		}
	}
	
}
